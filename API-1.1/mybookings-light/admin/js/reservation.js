jQuery.fn.print=function(){if(this.size()>1)return this.eq(0).print(),void 0;if(this.size()){var a="printer-"+(new Date).getTime(),b=jQuery("<iframe name='"+a+"'>");b.css("width","1px").css("height","1px").css("position","absolute").css("left","-9999px").appendTo(jQuery("body:first"));var c=window.frames[a],d=c.document,e=jQuery("<div>").append('<style type="text/css">*, * * {-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;}body {font-family:verdana;font-size:14px;}a{color:#000;text-decoration:none;}.table{width:100%;margin-bottom:20px;} .table thead {display:table-header-group;vertical-align:top;}.table th {padding:8px;line-height:20px;font-weight:700;}.text-right {text-align:right !important;} .table td {pading:8px;font-weight:400;border-top:1px solid #000;}.row{margin:0 -15px;}.row:after{float:none;clear:both;line-height:0;display:table;content:"";}.col-sm-6, .col-md-6{float:left;width:50%;padding:0 20px;position:relative;}.block table{text-align:left;margin-bottom:15px}.block table tr td,.block table tr th{border-bottom:1px solid #ddd;border-right:1px solid #ddd;padding:10px;line-height:normal;text-align:left}body h2.subtitle{font-size:24px!important;border-bottom:none;padding:15px 0 0;font-weight:400;line-height:1.4rem;color:#6B645A!important;margin-bottom:15px}h2{margin-bottom:15px !important;}table.booktable{border-top:3px solid #6B645A;margin-bottom:20px;width:99%}table.booktable tr{border-bottom:1px solid #6B645A}table.booktable tr td{padding:5px 0}table.booktable tr td.heading{min-width:80px;font-weight:700;color:#6B645A}table.booktable tr.noborder{border-bottom:none}table.booktable tr td.pricetd{padding-right:3px}</style>');d.open(),d.write('<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">'),d.write("<html>"),d.write("<body>"),d.write("<head>"),d.write("<title>"),d.write(document.title),d.write("</title>"),d.write(e.html()),d.write("</head>"),d.write(this.html()),d.write("</body>"),d.write("</html>"),d.close(),c.focus(),c.print(),setTimeout(function(){b.remove()},6e4)}};
;

jQuery(document).ready(function($) {

     jQuery('#booking_date_from').datepicker({

        dateFormat : 'dd/mm/yy',
        onSelect: function (selected) {
            var dtMin = moment(selected, 'DD/M/YYYY').add(1, 'month');
            // console.log();
            var dd = dtMin._d.getDate();
            var mm = dtMin._d.getMonth();
            var y = dtMin._d.getFullYear();
            var dtFormatted = moment(dtMin).format('DD/M/YYYY');
            jQuery("#booking_date_to").datepicker("option", "minDate", dtFormatted);
        }

     });

    

     jQuery('#booking_date_to').datepicker({

        dateFormat : 'dd/mm/yy'

     });



     jQuery('#ngappdiv').show();

 

});



  

 var app =  angular.module('searchBooking', ['ngResource']); 

  app.controller('resultsCtl', ['$scope',"$http", function($scope, $http) {

           $scope.results     = []; 

           $scope.totalfound  = 0; 

           $scope.searching   = 1; 
           var selected = $scope.selected = [];
           $scope.table_tr_ref_id = '';
           $scope.table_tr_alt    = 0;
           $scope.checkedall = $scope.checkedany = 0;
           $scope.showarchive = 0;
           $scope.httprequest = 0;
           
           $scope.showrowdata = function(e){
             returnval = '';
             if(e){  
               returnval = 'hideel';
             }

             return returnval;
           }


           ///

          var updateSelected = function(action, id) {
            if (action === 'add' && $scope.selected.indexOf(id) === -1) {
              $scope.selected.push(id);
            }
            if (action === 'remove' && $scope.selected.indexOf(id) !== -1) {
              $scope.selected.splice($scope.selected.indexOf(id), 1);
            }
            if($scope.selected.length > 0){
              $scope.showarchive = 1;
            }else {
              $scope.showarchive = 0;
            }
          };

          $scope.updateSelection = function($event, id) {
            var checkbox = $event.target;
            var action = (checkbox.checked ? 'add' : 'remove');
            updateSelected(action, id);
          };

          $scope.selectAll = function($event) {
            var checkbox = $event.target;
            var action = (checkbox.checked ? 'add' : 'remove');
            for ( var i = 0; i < $scope.results.length; i++) {
              var entity = $scope.results[i];
              updateSelected(action, entity.booking_ref);
            }
          };

          $scope.getSelectedClass = function(entity) {
            return $scope.isSelected(entity.booking_ref) ? 'selected' : '';
          };

          $scope.isSelected = function(id) {
            return $scope.selected.indexOf(id) >= 0;
          };

          //something extra I couldn't resist adding :)
          $scope.isSelectedAll = function() {
            return $scope.selected.length === $scope.results.length;
          };
           // 

           
           $scope.init= function(){
            $scope.httprequest = 1;
           $http.get(reservation_object.url+'?action='+reservation_object.name+'_search_bookings&date_from='+ jQuery('#booking_date_from').val()+'&date_to='+jQuery('#booking_date_to').val())

            .success(function(data, status) { 
                  newdata    = data;
                  old_ref_id = '';
                  table_tr_alt = 0;
                  jQuery.each( data, function( key, value ) {
                      if(value.booking_ref != old_ref_id){
                        old_ref_id = value.booking_ref;
                        newdata[key].same_ref_id  = false;
                        table_tr_alt++;
                        newdata[key].table_tr_alt  = (table_tr_alt) % 5;

                      }else{
                        newdata[key].same_ref_id  = true;
                        newdata[key].table_tr_alt  = (table_tr_alt) % 5;
                      }

                  });
                  

                  $scope.results = newdata;  

                  $scope.totalfound = data.length;  

                  $scope.searching  = $scope.checkedall = $scope.httprequest = 0;

            }); 
          }
          $scope.init();


            $scope.search_bookings = function(){

                 $scope.searching  = 1;

                 $http.get(reservation_object.url+'?action='+reservation_object.name+'_search_bookings&date_from='+ jQuery('#booking_date_from').val()+'&date_to='+jQuery('#booking_date_to').val())

                  .success(function(data, status) { 

                        newdata    = data;
                        old_ref_id = '';
                        table_tr_alt = 0;
                        jQuery.each( data, function( key, value ) {
                            if(value.booking_ref != old_ref_id){
                              old_ref_id = value.booking_ref;
                              newdata[key].same_ref_id  = false;
                              table_tr_alt++;
                              newdata[key].table_tr_alt  = (table_tr_alt) % 5;

                            }else{
                              newdata[key].same_ref_id  = true;
                              newdata[key].table_tr_alt  = (table_tr_alt) % 5;
                            }

                        });
                        

                        $scope.results = newdata;   

                        $scope.totalfound = data.length; 

                        $scope.searching  = $scope.checkedall = 0;

                  }); 

            }

            // $scope.checkall = function(e){
            //     $scope.checkedall = ($scope.checkedall) ? 0 : 1;
            // }

            $scope.bookingArchived = function(){
              var data = {
                'action'      : reservation_object.name+'_request_archive',
                'booking_ref' : $scope.selected
              };
              $scope.httprequest = 1;
              $http({
                url: reservation_object.url,
                method: 'POST',
                data: jQuery.param(data),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
              }).success(function(data,status){
                var scope = angular.element('[ng-controller=resultsCtl]').scope();
                scope.init();
                scope.selected = [];
                scope.httprequest = 0;
              }).error(function(){
                alert('error');
                var scope = angular.element('[ng-controller=resultsCtl]').scope();
                    scope.httprequest = 0;
              })
            }

   }]); 


var searchall =  angular.module('searchallBooking', ['ngResource']); 

  searchall.controller('resultsCtlall', ['$scope',"$http", function($scope, $http) {

           $scope.results     = []; 

           $scope.totalfound  = 0; 

           $scope.searching   = 1; 
           var selected = $scope.selected = [];
           $scope.table_tr_ref_id = '';
           $scope.table_tr_alt    = 0;
           $scope.checkedall = $scope.checkedany = 0;
           $scope.showarchive = 0;
           
           $scope.showrowdata = function(e){
             returnval = '';
             if(e){  
               returnval = 'hideel';
             }

             return returnval;
           }
           
           $scope.init= function(){
           $http.get(reservation_object.url+'?action='+reservation_object.name+'_search_bookings&getall=true&date_from='+ jQuery('#booking_date_from').val()+'&date_to='+jQuery('#booking_date_to').val())

            .success(function(data, status) { 
                  newdata    = data;
                  old_ref_id = '';
                  table_tr_alt = 0;
                  jQuery.each( data, function( key, value ) {
                      if(value.booking_ref != old_ref_id){
                        old_ref_id = value.booking_ref;
                        newdata[key].same_ref_id  = false;
                        table_tr_alt++;
                        newdata[key].table_tr_alt  = (table_tr_alt) % 5;

                      }else{
                        newdata[key].same_ref_id  = true;
                        newdata[key].table_tr_alt  = (table_tr_alt) % 5;
                      }

                  });
                  

                  $scope.results = newdata;  

                  $scope.totalfound = data.length;  

                  $scope.searching  = $scope.checkedall = 0;

            }); 
          }
          $scope.init();


            $scope.search_bookings = function(){

                 $scope.searching  = 1;

                 $http.get(reservation_object.url+'?action='+reservation_object.name+'_search_bookings&date_from='+ jQuery('#booking_date_from').val()+'&date_to='+jQuery('#booking_date_to').val())

                  .success(function(data, status) { 

                        newdata    = data;
                        old_ref_id = '';
                        table_tr_alt = 0;
                        jQuery.each( data, function( key, value ) {
                            if(value.booking_ref != old_ref_id){
                              old_ref_id = value.booking_ref;
                              newdata[key].same_ref_id  = false;
                              table_tr_alt++;
                              newdata[key].table_tr_alt  = (table_tr_alt) % 5;

                            }else{
                              newdata[key].same_ref_id  = true;
                              newdata[key].table_tr_alt  = (table_tr_alt) % 5;
                            }

                        });
                        

                        $scope.results = newdata;   

                        $scope.totalfound = data.length; 

                        $scope.searching  = $scope.checkedall = 0;

                  }); 

            }

            

   }]); 

jQuery(document).on('click','input[name="receiver_email"]',function(e){
  if(jQuery(this).val() == 'custom' && jQuery(this).is(":checked") ){
    jQuery(this).closest('form').find('input[name="bookingemail"]').stop().show();
  }else {
    jQuery(this).closest('form').find('input[name="bookingemail"]').stop().hide();
  }
});

jQuery(document).on('click','.savebookingsettings',function(e){
  e.preventDefault();
  var form = jQuery(this).closest('form');
  jQuery.ajax({
    type: form.attr('method'),
    url:reservation_object.url,
    data: form.serialize(),
    error: function(){
      form.find('.spinner').remove();
      alert('error');
    },
    dataType:'json',
    beforeSend: function(){
      form.append('<span class="spinner" />');
    },
    success: function(response){
      form.find('.spinner').remove();
      if(response){
        alert('option saved');
      }
    }
  })
});

var globalisconfirm = 0;



jQuery(document).on('click','.bookingAction', function(e){
     c = confirm('Are You Sure ?');
     if(c){ 
                  var v = jQuery(this),
                    data = JSON.parse(v.attr('data-info'));
                    jQuery.ajax({
                      type:'post',
                      url:reservation_object.url,
                      data:data,
                      beforeSend: function(){
                        v.closest('tr').append('<span class="spainner" />');
                      },
                      dataType:'json',
                      success:function(json){
                        v.closest('tr').find('.spainner').remove();
                        if(json.success){
                          //v.closest('tr').find('td.bookstatus').html(json.message); 
                          jQuery('.book-ref-'+ data.ref_id).html(json.message);

                         var scope = angular.element('[ng-controller=resultsCtl]').scope();
                             
                            scope.$apply(function(){
                                 for(i=0; i< scope.results.length; i++){
                                     if(scope.results[i].booking_ref == data.ref_id){
                                       newstatus = (data.action == 'bookingCancelled') ? 'Cancelled' : 'Confirmed';
                                       scope.results[i].status = newstatus;
                                     }
                                 }
                           });


                        }else {
                          alert(json.error);
                        }
                      }
                }).fail(function(){
                  alert('update, failed, please try again.')
                  v.closest('tr').find('.spainner').remove();
                }).error(function(){
                  alert('error');
                  v.closest('tr').find('.spainner').remove();
                }) 
              }

});

jQuery(document).on('click','.thankprint',function(e){
  e.preventDefault();
  jQuery("#bookOrder").print();
});