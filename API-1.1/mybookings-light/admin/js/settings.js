/**
 * Callback function for the 'click' event of the 'Set Footer Image'
 * anchor in its meta box.
 *
 * Displays the media uploader for selecting an image.
 *
 * @since 0.1.0
 */
function renderMediaUploader(id, name) {
    'use strict';
 
    var file_frame, image_data;
 
    /**
     * If an instance of file_frame already exists, then we can open it
     * rather than creating a new instance.
     */
    if ( undefined !== file_frame ) {
 
        file_frame.open();
        return;
 
    }
 
    /**
     * If we're this far, then an instance does not exist, so we need to
     * create our own.
     *
     * Here, use the wp.media library to define the settings of the Media
     * Uploader. We're opting to use the 'post' frame which is a template
     * defined in WordPress core and are initializing the file frame
     * with the 'insert' state.
     *
     * We're also not allowing the user to select more than one image.
     */
    file_frame = wp.media.frames.file_frame = wp.media({
        title:'Upload Image',
        button: {
          text:'Insert Image',
        },
        multiple: false
    });
 
    /**
     * Setup an event handler for what to do when an image has been
     * selected.
     *
     * Since we're using the 'view' state when initializing
     * the file_frame, we need to make sure that the handler is attached
     * to the insert event.
     */
    file_frame.on( 'insert', function() {
        
        /**
         * We'll cover this in the next version.
         */
 
    });
    file_frame.on('select', function(){
      image_data = file_frame.state().get( 'selection' ).first().toJSON();
      id.closest('.room_images').find('input[name*="room_images[]"]').val(image_data.id).end().find('.room_img').css({backgroundImage:'url('+image_data.url+')'}).html('<div class="image_editor_box"><a href="JavaScript:void(0)" class="'+name+'_cb_hm_change_image cb_hm_change_image">change</a><a href="JavaScript:void(0)" class="'+name+'_cb_hm_remove_image cb_hm_remove_image"><i class="fa fa-trash"></i></a></div>');
      // id.closest('.room_images')
      for ( var image_property in image_data ) {
    
        /**
         * Here, you have access to all of the properties
         * provided by WordPress to the selected image.
         *
         * This is generally where you take the data and so whatever
         * it is that you want to do.
         *
         * For purposes of example, we're just going to dump the
         * properties into the console.
         */
        // console.log( image_property + ': ' + image_data[ image_property ] );
 
      }
    });
 
    // Now display the actual file_frame
    file_frame.open();
 
}
 
(function( $ ) {
    'use strict';
 
    $(function() {
        jQuery(document).on('click', '.'+cb_hm_settings_obj.name+'_cb_hm_upload_image_button, .'+cb_hm_settings_obj.name+'_cb_hm_change_image',function(evt) { 
            // Stop the anchor's default behavior
            evt.preventDefault();
            var v = jQuery(this);
 
            // Display the media uploader
            renderMediaUploader(v, cb_hm_settings_obj.name);
 
        });
        jQuery(document).on('click','.'+cb_hm_settings_obj.name+'_cb_hm_remove_image', function(e){
          e.preventDefault();
          jQuery(this).closest('.room_images').find('input.uploadfield').val('');
          jQuery(this).closest('.room_img').css({backgroundImage:'none'}).html('<a href="javscript:void(0)" class="'+cb_hm_settings_obj.name+'_cb_hm_upload_image_button cb_hm_uploader"></a>');
        });

        jQuery(".qtip-tooltip").each(function() { // Notice the .each() loop, discussed below
            $(this).qtip({
                content: {
                    text: $(this).next('div') // Use the "div" element next to this for the content
                },
                show: {
                    event: 'mouseenter click'
                },
                hide: {
                    event: 'click unfocus'
                },
                style: {
                    classes: 'qtip-bootstrap'
                }
            });
        });
    });

    jQuery(document).on('click', '.hm_tab_panel a', function(e){
      e.preventDefault();
      if(!jQuery(this).hasClass('disabled')){
          var active = jQuery(this).attr('rel');
          jQuery(this).closest('ul').find('a.active').removeClass('active');
          jQuery(this).addClass('active');
          jQuery('#cb_hm_tab_content >.cb_hm_tabs').stop().hide();
          jQuery('#cb_hm_tab_content').find('#'+active).stop().show();
        }
    });

    jQuery(document).on('click', '.accordionhead >label', function(e){
        var v = jQuery(this).find('input');
        if(v.is(':checked')){
            v.closest('.accordion').find('.accordioncontent').stop().slideUp(300);
            v.closest('.accordiontab').find('.accordioncontent').stop().slideDown(300);
            if(v.val() == 'yes'){
                jQuery(".hm_tab_panel a").addClass('disabled');
                jQuery('.hm_tab_panel ul >li:eq(0)').find('a').removeClass('disabled');
                // jQuery('.hm_tab_panel >li:eq(3)').find('a').removeClass('disabled');
            }
        }else {
            jQuery('.hm_tab_panel a').removeClass('disabled');
            v.closest('.accordiontab').find('.accordioncontent').stop().slideUp(300);
        }
    });
 
})( jQuery );