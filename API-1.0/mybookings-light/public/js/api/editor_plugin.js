(function() {
  tinymce.create('tinymce.plugins.cloudbeds', {
    init : function(ed, url) {  

       ed.addCommand('open_cloudbeds_frame', function(){  
            window.tb_remove(); 

              setTimeout(function() {
                  tb_show( 'Cloudbeds Shortcode', '#TB_inline?width=' + 900 + '&height=' + 200 +'&inlineId=cloudbeds_shortcode_dialog')
              }, 500);
        });


      ed.addButton('cloudbeds', {
        title : 'cloudbeds',
        image : url+'/cloudbeds_dark.png',
        cmd   : 'open_cloudbeds_frame', 
      });
 

    },
    createControl : function(n, cm) {
      return null;
    },
    getInfo : function() {
      return {
        longname : "Cloudbeds Shortcode",
        author : 'enblochotels',
        authorurl : 'http://enblochotels.com/',
        infourl : 'http://enblochotels.com/',
        version : "1.0"
      };
    }
  });
  tinymce.PluginManager.add('cloudbeds', tinymce.plugins.cloudbeds);
})();

function do_insert_cloudbeds_shortcode(){
    pid = parseInt(jQuery('#cloudbeds_property_id').val()); 
    if(pid){  
         tinymce.execCommand('mceInsertContent', false, '[cloudbeds id="'+pid+'"]'); 
         window.tb_remove(); 
    }else{
         alert('Please give a vaild Property ID')
    } 
} 