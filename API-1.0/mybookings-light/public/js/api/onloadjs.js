 
jQuery(function($){
    OFBooking = {
        initBooking: function(
            //Datepicker
            urls,minDate, formats, adults, kids, start_date, end_date,
            //Facebook
            fb_app,
            // Validation parameters
            boletoDate, eBankingDate,
            // Error Messages
            disabled_msg, no_rooms_msg, not_enough_rooms_msg, no_rooms_selected_msg, boletoDateError, eBankingDateError,
            // Var initialization
            ct, roomCount, cartCount, dep_type, dep_pct, dep_val, taxes, latitude, longitude, hotel_name, fb, country,
            taxes_included, included_taxes_type, terms_tax_percent, terms_tax_fixed, fees_included, fees_included_type,
            fees_percent, fees_fixed,
            // DOM Templates
            newRoom, newPopup, newDetailedRatesPopup, newDetails, newDetailsPackage, newBookingRoom
            ){
            var _this = this;
            _this.promo_code = window.location.hash.replace('#/', '');
            $('#promo_code_input').val(_this.promo_code.toLowerCase());

            $('.userAgreementLink').click(function(event) {
                event.preventDefault();
                _this.showPopup('terms_popup');
            });
            this.initMap(latitude, longitude, hotel_name);

            formats.date_format_DP = formats.date_format
                .replace('y', 'yy')
                .replace('Y', 'yy')
                .replace('m', 'mm')
                .replace('d', 'dd');

            //
            // Popup and Spinners - START
            //

            function startSpinner(){
                var opts = {
                    lines: 17, // The number of lines to draw
                    length: 28, // The length of each line
                    width: 10, // The line thickness
                    radius: 40, // The radius of the inner circle
                    corners: 1, // Corner roundness (0..1)
                    rotate: 0, // The rotation offset
                    direction: 1, // 1: clockwise, -1: counterclockwise
                    color: '#000', // #rgb or #rrggbb or array of colors
                    speed: 1.2, // Rounds per second
                    trail: 57, // Afterglow percentage
                    shadow: false, // Whether to render a shadow
                    hwaccel: false, // Whether to use hardware acceleration
                    className: 'spinner', // The CSS class to assign to the spinner
                    zIndex: 2e9, // The z-index (defaults to 2000000000)
                    top: 'auto', // Top position relative to parent in px
                    left: 'auto' // Left position relative to parent in px
                };
                var target = $('.spinner_container');
                target.show();
                target.spin(opts);
            }

            function stopSpinner(){
                var target = $('.spinner_container');
                target.spin(false);
                target.hide();
            }

            function animateSegura() {
                var seguraColors = ['#c6e1ff', '#white'];
                for (var i = 0; i < 7; i++) {
                    $('.segura').animate({
                        backgroundColor: seguraColors[i % 2]
                    }, 300 );
                }
            }

            function reCalcSeguraPrice(node) {
                // var row = node.closest('tr');
                // var qty_s = $('select[name^="qty_rooms["]', row);
    //        if (qty_s.val() > 0) {
    //
    //        } else {
    //            $('.segura .compra, .segura .compra_price, .segura .compra_notice').text('');
    //        }
                var qty = 0;
                var total = 0;
                var dep = 0;
                var tax = 0;
                var roomCount = 0;

                var form = $("#reservationDetailsForm");
                var checkin = $('input[name="search_start_date"]').val();
                var checkout = $('input[name="search_end_date"]').val();
                var rm_cont = $('.selected_room_container');

                $('input[name$="selected_"]', form).remove();
                $('.short_info2', rm_cont).remove();

                $("select[name^='qty_rooms[']").each(function(index, element){
                    var selected = $(element).val();
                    roomCount += parseInt(selected);

                    if(selected > 0){
                        var row = $(element).closest('.room_type_row');
                        var id = row.data('id');
                        var adults = $('select[name^="adults["]', row).val();
                        var kids = $('select[name^="kids["]', row).val();
                        var val = parseCurrency(row.data('val'));
                        var adt = row.data('adults');
                        var kid = row.data('kids');
                        var add_adt = parseCurrency(row.data('add-adults'));
                        var add_kid = parseCurrency(row.data('add-kids'));
                        var name = row.data('name');

                        var room = val;

                        for(var i = 0; i < selected; i++){
                            if (adults > adt){
                                room += add_adt * (adults - adt);
                            }
                            if (kids > kid){
                                room += add_kid * (kids - kid);
                            }

                            var fixes_js_rounding = 0.0001;

                            var room_tax = 0;
                            var room_dep = 0;

                            if(dep_type == 'percent'){
                                room_tax = parseFloat(dep_pct * taxes * room + fixes_js_rounding).toFixed(2);
                                room_dep = parseFloat(dep_pct * room + fixes_js_rounding).toFixed(2);
                            } else {
                                var nights = $('.nights_count').text();
                                room_dep = (room < (dep_val*nights))?room:(dep_val*nights);
                            }

                            dep += parseFloat(room_tax) + parseFloat(room_dep);
                            tax += taxes * room;
                            total += room;
                        }

                        qty+=selected;
                        var rm_p = '<p class="short_info2">' +
                            name +
                            ' (x' + selected + ') <br />'+
                            ' <span class="bold">Adults:</span>' + adults + ', '+
                            ' <span class="bold">Children:</span>' + kids +
                            '</p>';
                        rm_cont.append(rm_p);

                        form.append('<input type="hidden" name="selected_room_qty['+id+']" value="'+selected+'" />');
                        form.append('<input type="hidden" name="selected_adults['+id+']" value="'+adults+'" />');
                        form.append('<input type="hidden" name="selected_kids['+id+']" value="'+kids+'" />');

                    }
                });

                if (total > 0) {
                    $('.segura .compra_rooms span.roomsCount').text(roomCount);
                    $('.segura .compra_price').text(formatCurrency(total));
                    $('.segura .compra_notice').removeClass('hide');
                    $('.segura .compra_rooms').removeClass('hide');
                    $('.segura .compra').removeClass('hide').addClass('hide');
                } else {
                    resetSeguraBlock();
                }
            }

            function resetSeguraBlock(){
                $('.segura .compra_price').text('');
                $('.segura .compra_notice').removeClass('hide').addClass('hide');
                $('.segura .compra_rooms').removeClass('hide').addClass('hide');
                $('.segura .compra').removeClass('hide');
            }

            $('.modal-overlay').click(function(event){
                if($(event.target).is('.modal-overlay')){
                    _this.hidePopup();
                }
            });

            $('.close_popup').click(function(){
                _this.hidePopup();
            });

            //
            // Popup and Spinners - END
            //



            //
            // Set Datepickers - START
            //

            $('.ofdate').datepicker({
                minDate: minDate,
                //    maxDate: maxDate,
                dateFormat: formats.date_format_DP,
                onSelect: function(){
                    if($(this).is('input[name="search_start_date"]')){
                        var sd = $(this).val();
                        var edf = $('input[name="search_end_date"]');
                        var ed = edf.val();

                        sd = new Date(validDate(sd).standardDate);
                        ed = new Date(validDate(ed).standardDate);

                        var nd = sd.setDate(sd.getDate()+1);
                        nd = new Date(nd);

                        edf.datepicker( "option", "minDate", nd );
                        setTimeout(function(){edf.datepicker("show")}, 100);

                        if(sd >= ed){
                            var day = nd.getDate();
                            if(day < 10) day = '0' + day;

                            var month = nd.getMonth() + 1;
                            if(month < 10) month = '0' + month;

                            if(formats.date_format_DP == 'dd/mm/yy')
                                edf.val(
                                    day + '/' + month + '/' +
                                        nd.getFullYear()
                                );
                            else
                                edf.val(
                                    month + '/' + day + '/' +
                                        nd.getFullYear()
                                );
                        }
                    }
                }
            });

            //saveReservationDates($('input[name="search_start_date"]').val(), $('input[name="search_end_date"]').val());
            //setCookieDates();

            $(".ofdate").blur(function(){
                $(this).val(validDate($(this).val()).convertedDate);
            });

            //
            // Set Datepickers - END
            //



            //
            // Validation Helper Functions - START
            //

            function validDate(value){
                var shortDateFormat = formats.date_format_DP;
                var res = true;
                var arr = value.split('/');
                try {
                    if(arr[2] < 100 && arr[2] >=13){
                        arr[2] = "20"+arr[2];
                        value = arr.join("/");
                    }

                    $.datepicker.parseDate(shortDateFormat, value);
                } catch (error) {
                    if(arr.length == 2){
                        arr[2] = (new Date()).getFullYear();
                        value = arr.join("/");

                        try{
                            $.datepicker.parseDate(shortDateFormat, value);
                        }
                        catch(error){
                            res = false;
                        }
                    } else {
                        res = false;
                    }
                }

                if(res){
                    var today = new Date((new Date()).getFullYear(), (new Date()).getMonth(), (new Date()).getDate());
                    var toCompare = new Date(arr[2], arr[0]-1, arr[1]);

                    if(formats.date_format_DP == 'dd/mm/yy')
                        toCompare = new Date(arr[2], arr[1]-1, arr[0]);

                    if(toCompare < today){
                        res = false;
                    }
                }

                var array = {}, d = null;
                array.result = res;
                array.convertedDate = value;

                if(shortDateFormat == 'dd/mm/yy'){
                    array.standardDate = arr[2]+'/'+arr[1]+'/'+arr[0];
                    d = new Date(arr[2], arr[1] - 1, arr[0]);
                    array.sameDate = (d && (d.getMonth() + 1) == arr[1] && d.getDate() == Number(arr[0]));
                }
                else{
                    array.standardDate = arr[2]+'/'+arr[0]+'/'+arr[1];
                    d = new Date(arr[2], arr[0] - 1, arr[1]);
                    array.sameDate = (d && (d.getMonth() + 1) == arr[0] && d.getDate() == Number(arr[1]));
                }

                return array;
            }

            function validCCDate(){
                var y_input = $('select[name="exp_year"]');
                var m_input = $('select[name="exp_month"]');
                var year = y_input.val();
                var month = m_input.val();

                var dt = new Date(year, month-1, 1, 0, 0, 0);
                var td = new Date();

                if(dt > td){
                    y_input.parent().removeClass('error').addClass('valid');
                    m_input.parent().removeClass('error').addClass('valid');
                } else {
                    y_input.parent().removeClass('valid').addClass('error');
                    m_input.parent().removeClass('valid').addClass('error');
                }

                return (dt > td);
            }

            //
            // Validation Helper Functions - END
            //



            //
            // jQuery Validate Extensions and Helpers - START
            //

            // Correct date validation error in Chrome and Safari.
            jQuery.validator.addMethod("ofdate", function(value) {
                var check = validDate(value);
                return check.result;
            }, "Date Invalid");

            jQuery.validator.addMethod("cvv", function(cvvCode) {
                if(!ct)
                    return true;

                var digits = 0;
                switch (ct) {
                    case 'amex':
                        digits = 4;
                        break;
                    default:
                        digits = 3;
                        break;
                }

                var regExp = new RegExp('[0-9]{' + digits + '}');
                return (cvvCode.length == digits && regExp.test(cvvCode));

            }, "CVV Invalid");

            jQuery.validator.addMethod("cc", function(value) {
                value = value.replace(/-/g, '');
                value = value.replace(/ /g, '');
                value = value.replace(/\./g, '');

                var __indexOf = [].indexOf || function(item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; };

                var card, card_type, card_types, get_card_type, is_valid_length, is_valid_luhn, normalize, validate, validate_number;
                card_types = [
                    {
                        name: 'amex',
                        pattern: /^3[47]/,
                        valid_length: [15]
                    }, {
                        name: 'diners_club_carte_blanche',
                        pattern: /^30[0-5]/,
                        valid_length: [14]
                    }, {
                        name: 'diners_club_international',
                        pattern: /^36/,
                        valid_length: [14]
                    }, {
                        name: 'jcb',
                        pattern: /^35(2[89]|[3-8][0-9])/,
                        valid_length: [16]
                    }, {
                        name: 'laser',
                        pattern: /^(6304|670[69]|6771)/,
                        valid_length: [16, 17, 18, 19]
                    }, {
                        name: 'visa_electron',
                        pattern: /^(4026|417500|4508|4844|491(3|7))/,
                        valid_length: [16]
                    }, {
                        name: 'visa',
                        pattern: /^4/,
                        valid_length: [16]
                    }, {
                        name: 'mastercard',
                        pattern: /^5[1-5]/,
                        valid_length: [16]
                    }, {
                        name: 'maestro',
                        pattern: /^(5018|5020|5038|6304|6759|676[1-3])/,
                        valid_length: [12, 13, 14, 15, 16, 17, 18, 19]
                    }, {
                        name: 'discover',
                        pattern: /^(6011|622(12[6-9]|1[3-9][0-9]|[2-8][0-9]{2}|9[0-1][0-9]|92[0-5]|64[4-9])|65)/,
                        valid_length: [16]
                    }
                ];

                get_card_type = function(number) {
                    var _j, _len1, _ref2;
                    _ref2 = (function() {
                        var _k, _len1, _results;
                        _results = [];
                        for (_k = 0, _len1 = card_types.length; _k < _len1; _k++) {
                            card = card_types[_k];

                            _results.push(card);
                        }
                        return _results;
                    })();
                    for (_j = 0, _len1 = _ref2.length; _j < _len1; _j++) {
                        card_type = _ref2[_j];
                        if (number.match(card_type.pattern)) {
                            return card_type;
                        }
                    }
                    return null;
                };

                is_valid_luhn = function(number) {
                    var digit, n, sum, _j, _len1, _ref2;
                    sum = 0;
                    _ref2 = number.split('').reverse();
                    for (n = _j = 0, _len1 = _ref2.length; _j < _len1; n = ++_j) {
                        digit = _ref2[n];
                        digit = +digit;
                        if (n % 2) {
                            digit *= 2;
                            if (digit < 10) {
                                sum += digit;
                            } else {
                                sum += digit - 9;
                            }
                        } else {
                            sum += digit;
                        }
                    }
                    return sum % 10 === 0;
                };

                is_valid_length = function(number, card_type) {
                    var _ref2;
                    return _ref2 = number.length, __indexOf.call(card_type.valid_length, _ref2) >= 0;
                };

                validate_number = function(number) {
                    var length_valid, luhn_valid;
                    card_type = get_card_type(number);
                    luhn_valid = false;
                    length_valid = false;
                    if (card_type != null) {
                        luhn_valid = is_valid_luhn(number);
                        length_valid = is_valid_length(number, card_type);
                    }

                    if(!card_type || !luhn_valid || !length_valid)
                        return false;

                    ct = card_type['name'];
                    return true;
                };

                validate = function() {
                    var number = value.replace(/[ .-]/g, '');
                    return validate_number(number);
                };

                normalize = function(number) {
                    return ;
                };

                return validate();
            }, "Card Invalid");

            jQuery.validator.addMethod("ccdate", function() {
                return validCCDate();
            }, "Date Invalid");

            function prepareFormValidation(form) {
                var f = $(form).get(0);
                $.removeData(f, 'validator');
                $(form).validate({
                    errorPlacement: function (error, element) { //hide error messages
                        var elem = $(element);

                        // Check we have a valid error message
                        if(!error.is(':empty')) {
    //                    console.debug(error);
                            elem.attr('error-message', error.html());
                        }

                        // If the error is empty, remove the qTip
                        else {
                            elem.qtip('destroy');
                            elem.removeAttr('error-message');
                        }
                    },
                    success: $.noop, // Odd workaround for errorPlacement not firing!
                    rules: {
                        cpf: {
                            cpf: true
                        },
                        zip: {
                            cep: true
                        },
                        search_start_date: {
                            ofdate: true
                        },
                        card_number: {
                            cc: true
                        },
                        cvv: {
                            cvv: true
                        },
                        exp_month: {
                            ccdate: true
                        },
                        exp_year: {
                            ccdate: true
                        }
                    }
                });
            }

            //
            // jQuery Validate Extensions and Helpers - END
            //



            //
            // Room Inventory Events and Helpers - START
            //

            $(document).on('click', '.name_room', function () {

                var nr = $(this);
                var open = nr.hasClass('for_minus');
                var row = nr.closest('.room_type_row');

                $('.name_room').removeClass('for_minus').addClass('for_plus');

                row.siblings('.info_about_rooms').find('.room_details_container').hide();
                row.siblings('.info_about_rooms').hide();
                fluidLayout();

                if(!open){
                    var tabs = $('.tab_info2 > div', row.next('.info_about_rooms'));

                    var m_h = 0;

                    tabs.each(function(index, element){
                        var clone = $(element).clone();

                        clone.css({
                            'opacity': 0,
                            'position': 'absolute',
                            'top': 0,
                            'max-width': 769
                        })
                            .appendTo('body');

                        var tmp_height = clone.height();
                        if(tmp_height > m_h)
                            m_h = tmp_height;
                        clone.remove();
                    });

                    tabs.css('height', m_h);

                    nr.removeClass('for_plus').addClass('for_minus');
                    row.next('.info_about_rooms').show();

                    row.next('.info_about_rooms').find('.room_details_container').show();
                    fluidLayout();
                }

                return false;
            });

            $(document).on('click', 'ul.room_type_tabs li', function(){
                var details = $(this).closest('.info_about_rooms');
                var li = $('ul.room_type_tabs li', details);
                var idx = $(this).index();
                var max = li.length-1;
                if(idx > 0 && idx < max){
                    idx--;
                    li.removeClass('active');
                    $(this).addClass('active');
                    $('.tab_info2 > div', details).removeClass('active');
                    $('.tab_info2 > div:eq('+idx+')', details).addClass('active');

                    fluidLayout();
                }
            });

            $(document).on("mouseover", ".room_name_container", function(){
                $('.stay', this).removeClass('hide');
            });

            $(document).on("mouseout", ".room_name_container", function(){
                $('.stay', this).addClass('hide');
            });

            $(document).on("mouseover", ".avg_rate_container", function(){
                $('.stay.detailed_rates', this).removeClass('hide');
            });

            $(document).on("mouseout", ".avg_rate_container", function(){
                $('.stay.detailed_rates', this).addClass('hide');
            });

            function generate_dropdown_options(start, max, selected) {
                var options = '';

                var first = start;
                var min = start;
                if (isNaN(start)) {
                    min = 0;
                }

                for (var i_a = min; i_a <= max; i_a++) {
                    options += '<option value="';

                    if (i_a == min && isNaN(start))
                        options += '"';
                    else
                        options += i_a + '"';

                    if (selected && i_a == selected)
                        options += ' selected="selected"';

                    options += '>';

                    if (i_a == min)
                        options += start;
                    else
                        options += i_a;

                    options += '</option>';

                }
                return options;
            }

            $('.book_now').click(function () {
                var qty = 0;
                var total = 0;
                var dep = 0;
                var total_tax = 0;

                var sub_tax = 0;
                var sub_fees = 0;

                var form = $("#reservationDetailsForm");
                var checkin = $('input[name="search_start_date"]').val();
                var checkout = $('input[name="search_end_date"]').val();
                var rm_cont = $('.selected_room_container');

                $('input[name$="selected_"]', form).remove();
                $('.short_info2', rm_cont).remove();

                $("select[name^='qty_rooms[']").each(function(index, element){
                    var selected = $(element).val();

                    if(selected > 0){
                        var row = $(element).closest('.room_type_row');
                        var id = row.data('id');
                        var adults = $('select[name^="adults["]', row).val();
                        var kids = $('select[name^="kids["]', row).val();
                        var val = parseCurrency(row.data('val'));
                        var adt = row.data('adults');
                        var kid = row.data('kids');
                        var add_adt = parseCurrency(row.data('add-adults'));
                        var add_kid = parseCurrency(row.data('add-kids'));
                        var name = row.data('name');

                        var room = val;

                        var fixes_js_rounding = 0.0001;

                        for(var i = 0; i < selected; i++){
                            if (adults > adt){
                                room += add_adt * (adults - adt);
                            }
                            if (kids > kid){
                                room += add_kid * (kids - kid);
                            }

                            var room_tax = 0;
                            var room_dep = 0;

                            if(dep_type == 'percent'){

                                if (taxes_included == 'Y' && included_taxes_type == 'percent')
                                    room_tax += parseFloat(parseFloat(dep_pct *  terms_tax_percent * room + fixes_js_rounding).toFixed(2));

                                if (fees_included == 'Y' && fees_included_type == 'percent')
                                    room_tax += parseFloat(parseFloat(dep_pct * fees_percent * room + fixes_js_rounding).toFixed(2));

                                room_dep = parseFloat(dep_pct * room + fixes_js_rounding).toFixed(2);
                            } else {
                                var nights = $('.nights_count').text();
                                room_dep = (room < (dep_val*nights))?room:(dep_val*nights);
                            }

                            dep += parseFloat(room_tax) + parseFloat(room_dep);
                            total += room;
                        }

                        if(dep_type == 'percent'){
                            var tmp_tax = 0;
                            if (taxes_included == 'Y' && included_taxes_type == 'fixed')
                                tmp_tax += terms_tax_fixed;
                            if (fees_included == 'Y' && fees_included_type == 'fixed')
                                tmp_tax += fees_fixed;

                            dep += parseFloat(parseFloat(dep_pct * tmp_tax + fixes_js_rounding).toFixed(2));
                        }


                        if (taxes_included == 'Y')
                            sub_tax =  included_taxes_type == 'percent'? total * terms_tax_percent : terms_tax_fixed;

                        if (fees_included == 'Y')
                            sub_fees =  fees_included_type == 'percent'? total * fees_percent : fees_fixed;

                        total_tax = parseFloat(sub_tax.toFixed(2)) + parseFloat(sub_fees.toFixed(2));

                        qty+=selected;
                        var rm_p = $('.clonable_for_selected_room_container').clone().removeClass('hide clonable_for_selected_room_container');
                        $('.selected_room_container_name', rm_p).html(name);
                        $('.selected_room_container_selected_count', rm_p).html(selected);
                        $('.selected_room_container_adults', rm_p).html(adults);
                        $('.selected_room_container_kids', rm_p).html(kids);
                        rm_cont.append(rm_p);
                        form.append('<input type="hidden" name="selected_room_qty['+id+']" value="'+selected+'" />');
                        form.append('<input type="hidden" name="selected_adults['+id+']" value="'+adults+'" />');
                        form.append('<input type="hidden" name="selected_kids['+id+']" value="'+kids+'" />');
                    }
                });

                if(qty==0){
                    alert(no_rooms_selected_msg);
                    return;
                }

                var days = calculateDays(checkin, checkout);

                $('.checkin_date').text(checkin);
                $('.checkout_date').text(checkout);
                $('.nights_int').text(days);
                $('.total_value').text(formatCurrency(total));

                if (taxes_included == 'Y')
                    $('.sub_tax').text(formatCurrency(sub_tax));

                if (fees_included == 'Y')
                    $('.sub_fees').text(formatCurrency(sub_fees));
                else
                    $('.sub_fees_row').remove();

                prepareFormValidation($('#reservationDetailsForm'));
                checkPaymentDate();

                $('.total_taxes').text(formatCurrency(total_tax));
                $('.total_deposit').text(formatCurrency(dep));
                $('.total_checkin').text(formatCurrency(total + total_tax - dep));
                $('.grand_total').text(formatCurrency(total + total_tax));

                form.append('<input type="hidden" name="selected_checkin" value="'+checkin+'" />');
                form.append('<input type="hidden" name="selected_checkout" value="'+checkout+'" />');

                of_hide($('.chooser'), function(){
                    of_hide($('.search_panel'), function(){
                        of_show($('.suas'), function(){
                            $('.change_reserve').show();
                            fluidLayout();
                        })
                    });
                });
            });

            $('.country_selector').change(function () {
                if($(this).val() == country){
                    $('div.data').show();
                    $('div.cpp').show();
                    $('div.sexo').show();

                    $('div.telefone').show();
                    $('div.telefone input').addClass('required');
                    $('div.celular').show();
                    $('div.celular input').addClass('required');

                    $('div.hear_about').show();

                    $('div.rua').show();
                    $('div.rua input').addClass('required');
                    $('div.numero').show();
                    $('div.numero input').addClass('required');
                    $('div.complemento').show();

                    $('div.bairro').show();
                    $('div.bairro input').addClass('required');
                    $('div.cidade').show();
                    $('div.cidade input').addClass('required');
                    $('div.estado').show();
                    $('div.cep').show();

                    $('hr.hide_line').show();

                    $('div.rg').show();
                    $('div.data_emissao').show();

                    $('div.orgao').show();
                    $('div.estade_emissao').show();

                    $('div.radio_block').show();

                    $('div.zip_foreign').hide();
                } else {
                    $('div.data').hide();
                    $('div.data.de').show(); // Expiration Date

                    $('div.cpp').hide();
                    //$('div.sexo').hide();

                    //$('div.telefone').hide();
                    $('div.telefone input').removeClass('required error');
                    //$('div.celular').hide();
                    $('div.celular input').removeClass('required error');

                    //$('div.hear_about').hide();
                    //$('div.hear_other_how').hide();

                    //$('div.rua').hide();
                    $('div.rua input').removeClass('required error');
                    //$('div.numero').hide();
                    $('div.numero input').removeClass('required error');
                    //$('div.complemento').hide();

                    //$('div.bairro').hide();
                    $('div.bairro input').removeClass('required error');
                    //$('div.cidade').hide();
                    $('div.cidade input').removeClass('required error');
                    $('div.estado').hide();
                    $('div.cep').hide();

                    $('hr.hide_line').hide();

                    $('div.rg').hide();
                    $('div.data_emissao').hide();

                    $('div.orgao').hide();
                    $('div.estade_emissao').hide();

                    $('input[name="payment_method"][value="cards"]').click();
                    $('div.radio_block').hide();

                    $('div.zip_foreign').show();
                }
            });


            function of_hide(element, callback){
                element.hide({
                    effect: 'blind',
                    easing: 'swing',
                    duration: 'slow',
                    complete: function(){
                        if(undefined != callback)
                            callback();
                    }
                });

                if(undefined != callback)
                    callback();
            }

            function of_show(element, callback){
                element.show({
                    effect: 'blind',
                    easing: 'swing',
                    duration: 'slow',
                    complete: function(){
                        if(undefined != callback)
                            callback();
                    }
                });

                if(undefined != callback)
                    callback();
            }

            //
            // Room Inventory Events and Helpers - END
            //



            //
            // Form Field Events - START
            //

            $('input.pesquisar, input.submit_code').click(searchAvailRooms);

            $('.change_reserve').click(function () {
                of_hide($('.suas'), function(){
                    of_show($('.chooser'), function(){
                        of_show($('.search_panel'), function(){
                            $('.change_reserve').hide();
                            fluidLayout();
                        });
                    });
                });
            });

            $(document).on('change', 'select[name="search_adults"], select[name="search_kids"], '+
                'input[name="search_start_date"], input[name="search_end_date"], '+
                'select[name="search_rooms"]', function () {
                of_hide($('.chooser'));
                $('.best_price').toggle(false);
                $('.segura').css('min-height', 182);
            });

            $(document).on('change', 'input[name="search_start_date"]', function () {
                var sd = $(this).val();
                var ed = $('input[name="search_end_date"]').val();

                sd = new Date(validDate(sd).standardDate);
                ed = new Date(validDate(ed).standardDate);

                if(sd >= ed){
                    var nd = sd.setDate(sd.getDate()+1);
                    nd = new Date(nd);

                    var day = nd.getDate();
                    if(day < 10) day = '0' + day;

                    var month = nd.getMonth() + 1;
                    if(month < 10) month = '0' + month;

                    $('input[name="search_end_date"]').val(
                        day + '/' + month + '/' +
                            nd.getFullYear()
                    );
                }
            });

            function searchAvailRooms(){
                $('#reservationDetailsForm input[type=hidden][name^=selected_]').remove();

                $('.best_price').toggle(false);
                $('.segura').css('min-height', 182);
                var promo_code = $('#promo_code_input').val().toLowerCase();
                $('#promo_code_input').val(promo_code);
                if (promo_code != ''){
                    $.ajax({
                        url:'/sites/check_promo_code',
                        data: {
                            'promo_code': promo_code
                        },
                        dataType: 'JSON',
                        type: 'POST',
                        success: function(response) {
                            if (response.status == 'exist') {
                                correct_promo(promo_code);
                            } else {
                                show_promo_error(promo_code);
                            }
                        },
                        error: function(reasone) {
                            show_promo_error(promo_code);
                        }
                    });
                }
                else{
                    correct_promo(promo_code);
                }
            }

            function show_promo_error(promo_code) {
                $('#promo_code_input').val('');
                window.location.href = '#/';
                $('.promo_error .popup-title label').text(promo_code);
                _this.showPopup('promo_error');
            }

            function correct_promo(promo_code){
                if (window.location.href != '#/' + promo_code) {
                    window.location.href = '#/' + promo_code;
                    _this.promo_code = promo_code.toLowerCase();
                }
                resetSeguraBlock();
                updateAvailableRoomsList();
            }

            function calculateDays(checkin, checkout){
                var sd = validDate(checkin),
                    sd_standard = sd.standardDate;

                sd = sd.convertedDate;

                var ed = validDate(checkout),
                    ed_standard = ed.standardDate;

                ed = ed.convertedDate;

                var ed_standard_split = getDateComponents(ed_standard, '/'),
                    ed_standard_date = (new Date(ed_standard_split[0], ed_standard_split[1], ed_standard_split[2])).getTime(),
                    sd_standard_split = getDateComponents(sd_standard, '/'),
                    sd_standard_date = (new Date(sd_standard_split[0], sd_standard_split[1], sd_standard_split[2])).getTime();

                return parseInt((ed_standard_date-sd_standard_date)/(1000*60*60*24));
            }

            function updateAvailableRoomsList(callback) {
                var msg_container = $('.message_container');
                var room_container = $('.room_container');
                var room_table = $('table tbody', room_container);

                room_table.html('');
                startSpinner();
                $('.chooser').hide();

                fluidLayout();

                if ($('#searchRoomForm').valid()) {
                    var _stDate = $('input[name="search_start_date"]').val(),
                        _endDate = $('input[name="search_end_date"]').val();

                    var days = calculateDays(_stDate, _endDate);

                    var sd = validDate(_stDate).convertedDate;
                    var ed = validDate(_endDate).convertedDate;

                    if(days < 1){
                        $(room_container).hide();
                        $('.chooser').hide();
                        $(msg_container).hide();
                        stopSpinner();

                        alert('Dade Invalid!!!');

                        return false;
                    }

                    $('.nights_count').text(days);

                    var rooms = 1;
                    var max_rooms = 5;
                    var ad = 1;
                    var kd = 0;
                    var widget_property = $('input[name="widget_property"]').val();
                    var lang = $('input[name="lang"]').val();
                    $.ajax({
                        url: urls.rooms,
                        data: {
                            start_date: sd,
                            end_date: ed,
                            widget_property: widget_property,
                            adults: ad,
                            kids: kd,
                            lang: lang,
                            date_format: formats.date_format
                        },
                        type: 'post',
                        dataType: 'json',
                        success: function (data) {
                            stopSpinner();
                            if (data.enabled == 0) {
                                var disabled = '<p class="sorry">'+disabled_msg+'</p>';
                                msg_container.html(disabled);

                                $(room_container).hide();
                                $('.chooser').show();
                                $(msg_container).show();

                                return;
                            }
                            if (data.success == false) {
                                if(!data.message)
                                    data.message = no_rooms_msg;

                                var err_message = '<p class="sorry">' + data.message + '</p>';
                                msg_container.html(err_message);

                                $(room_container).hide();
                                $('.chooser').show();
                                $(msg_container).show();

                                return;
                            }

                            msg_container.html('');


                            for (var room in data.room_types) {
                                var selected_room = data.room_types[room];

                                if (selected_room.num_available_now > 0) {
                                    if (_this.promo_code == '') {
                                        renderRoomRow(room_table, room_container, msg_container,  selected_room, max_rooms);
                                    }
                                    if (selected_room.packages.length){
                                        $.each(selected_room.packages, function(index, value){
                                            if (value.promo_code.toLowerCase() == _this.promo_code.toLowerCase()) {
                                                renderRoomRow(room_table, room_container, msg_container,  selected_room, max_rooms, value);
                                            }
                                        });
                                    }
                                }
                            }

                            if (data.total == 0) {
                                var norooms = '<p>'+no_rooms_msg+'</p>';

                                msg_container.html(norooms);

                                $(room_container).hide();
                                $('.chooser').show();
                                $(msg_container).show();

                                return;
                            }
                            else if (data.total < rooms) {
                                var notenoughrooms = '<p class="sorry">'+not_enough_rooms_msg+'</p>';

                                msg_container.html(notenoughrooms);

                                $(room_container).hide();
                                $('.chooser').show();
                                $(msg_container).show();

                                return;
                            }

                            fluidLayout();

                            if (undefined != callback)
                                callback();
                        }
                    });
                } else {
                    $(room_container).hide();
                    $(msg_container).hide();
                    stopSpinner();

                    alert('Dados Invalidos, por favor verifique os dados informados');
                    return false;
                }
            }

            function renderRoomRow(room_table, room_container, msg_container, selected_room, max_rooms, package){
                var other_photos = '';

                for (var p in selected_room.other_photos) {
                    var photo = selected_room.other_photos[p];
                    photo = photo.substring(0, photo.length-4) + '_thumb' + photo.substring(photo.length-4);
                    photo = "url('/" + photo + "')"
                    other_photos += '<div class="small"><div style="background-image: ' + photo + ';" class="room_image"> </div></div>';
                }
                selected_room.featured_photo = selected_room.featured_photo.replace('_featured.', '_thumb.');

                var max_avail_rooms = !package? selected_room.num_available_now: (package.rates[0].is_all_room == '1'? selected_room.num_available_now: parseInt(package.rates[0].count_rooms)),
                    roomCount = (max_avail_rooms < max_rooms)? max_avail_rooms: max_rooms;

                var roomOptions = generate_dropdown_options(0, roomCount, 0);
                var adOptions = generate_dropdown_options(1, selected_room.max_guests, 1);
                var kdOptions = generate_dropdown_options(0, selected_room.max_guests-1, 0);

                var curRoom = newRoom;
                var curDetails = package? newDetailsPackage: newDetails;
                var curPopup = newPopup;
                var curRates = newDetailedRatesPopup;

                var min = parseFloat(parseCurrency(selected_room.rate_min));

                var max_guests = "";

                if(selected_room.max_guests > 0){
                    for(var i = 0; i < selected_room.max_guests; i++){
                        max_guests+='<img src="/assets/booking/images/guest.png" class="guests_icon" />';
                    }
                }

                if(selected_room.num_available_now <= 3){
                    curRoom = curRoom.replace("{%room_type_only_left%}", selected_room.num_available_now);
                    curRoom = curRoom.replace("{%hide_class%}", '');
                } else {
                    curRoom = curRoom.replace("{%room_type_only_left%}", '');
                    curRoom = curRoom.replace("{%hide_class%}", ' hide');
                }

                var room_features = "";

                if(selected_room.features){
                    for(var f in selected_room.features){
                        room_features += "<li>- " + selected_room.features[f] + "</li>";
                    }
                }

                curPopup = curPopup.replace(/{%nights%}/g, selected_room.minday_rate);
                curPopup = curPopup.replace(/{%included_occupancy%}/g,
                    parseInt(selected_room.adults_inBasePrice) + parseInt(selected_room.children_inBasePrice)
                );
                curPopup = curPopup.replace(/{%maximum_occupancy%}/g, selected_room.max_guests);


                var ctt = '',
                    total_packages_price = 0,
                    min_packages_price = 0,
                    packages_prices = [];
                for(var r in selected_room.detailed_rates){
                    var rate = selected_room.detailed_rates[r];

                    var css = '';
                    if(r == 0) css+='padding-top-9 ';
                    if(r == selected_room.detailed_rates.length-1) css+='padding-bottom-14 ';

                    var package_day_price = 0;
                    if (package){
                        package_day_price = parseFloat(package.rates[0][package.rate_days[r]]);
                        total_packages_price += package_day_price;
                        packages_prices.push(package_day_price);
                    }

                    //alert(formats.date_format_DP);
                    var splitedDate = getDateComponents(rate.date);
                    ctt+='<tr class="' + css + '">';
                    ctt+='<td class="min_stay">' + formatDate(new Date(splitedDate[0], splitedDate[1], splitedDate[2]), formats.date_format_DP) + '</td>';
                    ctt+='<td class="nights">' + formatCurrency(!package? parseFloat(rate.rate): package_day_price) + '</td>';
                    ctt+='</tr>';
                }

                if (package){
                    packages_prices.sort(function(a, b){
                        return a >= b;
                    });
                    min_packages_price = packages_prices[0];
                }

                curRates = curRates.replace(/{%rates_top%}/g, (r*30 + 60));
                curRates = curRates.replace(/{%rates_content%}/g, ctt);

                curRoom = curRoom.replace(/{%room_type_name%}/g, !package? selected_room.room_type_title: selected_room.room_type_title +' ('+ package.name + ')');
                curRoom = curRoom.replace(/{%room_type_id%}/g, !package? selected_room.room_type_id:selected_room.room_type_id+'-'+package.package_id);
                curRoom = curRoom.replace(/{%featured_photo%}/g, (selected_room.featured_photo != '') ? '/' + selected_room.featured_photo : '/assets/reservation/images/room1.png');
                curRoom = curRoom.replace(/{%room_count%}/g, roomOptions);
                curRoom = curRoom.replace(/{%max-count-rooms%}/g, max_avail_rooms);
                curRoom = curRoom.replace(/{%adults_count%}/g, adOptions);
                curRoom = curRoom.replace(/{%kids_count%}/g, kdOptions);
                curRoom = curRoom.replace(/{%room_type_total_rate%}/g, formatCurrency(!package? parseFloat(parseCurrency(selected_room.rate_basic)): total_packages_price));
                curRoom = curRoom.replace(/{%room_type_min_rate%}/g, !package? formatCurrency(min): formatCurrency(min_packages_price));
                curRoom = curRoom.replace(/{%popup_info%}/g, curPopup);
                curRoom = curRoom.replace(/{%detailed_rates_popup%}/g, curRates);


                curRoom = curRoom.replace(/{%room_type_adult_rate%}/g, selected_room.rate_adult);
                curRoom = curRoom.replace(/{%room_type_child_rate%}/g, selected_room.rate_kid);

                curRoom = curRoom.replace(/{%adults_inBasePrice%}/g, selected_room.adults_inBasePrice);
                curRoom = curRoom.replace(/{%children_inBasePrice%}/g, selected_room.children_inBasePrice);

                curRoom = curRoom.replace(/{%max_guests%}/g, max_guests);
                curRoom = curRoom.replace(/{%max_guests_count%}/g, selected_room.max_guests);

                curRoom = curRoom.replace(/{%package_id%}/g, package? package.package_id: 0);


                /*
                 curRoom = curRoom.replace("{%room_type_value%}", "");
                 */

                curDetails = curDetails.replace("{%room_type_id%}", selected_room.room_type_id);
                curDetails = curDetails.replace("{%room_type_desc%}", selected_room.room_type_desc);

                if(package){
                    curDetails = curDetails.replace("{%package_src%}", '/'+package.path);
                    curDetails = curDetails.replace("{%package_desc%}", package.description);
                    curDetails = curDetails.replace("{%package_terms%}", package.terms);
                }

                curDetails = curDetails.replace("{%other_photos%}", other_photos);

                curDetails = curDetails.replace(/{%room_type_total_rate%}/g, selected_room.rate_basic);
                curDetails = curDetails.replace(/{%room_type_adult_rate%}/g, selected_room.rate_adult);
                curDetails = curDetails.replace(/{%room_type_child_rate%}/g, selected_room.rate_kid);

                curDetails = curDetails.replace(/{%adults_inBasePrice%}/g, selected_room.adults_inBasePrice);
                curDetails = curDetails.replace(/{%children_inBasePrice%}/g, selected_room.children_inBasePrice);

                room_table.append(curRoom);

                if(room_features){
                    curDetails = curDetails.replace(/{%room_type_features%}/g, room_features);
                    room_table.append(curDetails);
                    curDetails = $('tr.info_about_rooms').last();
                }
                else {
                    room_table.append(curDetails);
                    curDetails = $('tr.info_about_rooms').last();

                    // Get elements width, for future resizing
                    var amenities_width = $('li.amenities_tab', curDetails).outerWidth();
                    var last_width = $('ul.room_type_tabs li:last-child', curDetails).outerWidth();

                    // Remove Amenities tab and div
                    $('.amenities_tab', curDetails).remove();

                    // Adapt li, so last element takes over Amenities tab space
                    $('ul.room_type_tabs li:last-child', curDetails).css('width', amenities_width + last_width);
                }

                room_container.show();
                $('.chooser').show();
                msg_container.hide();
            }

            function fluidLayout(){
                var c = $('.choose_info');
                var s = $('.segura');
                c.height('auto');
                s.height('auto');
                var ch = c.height();
                var sh = s.height();

                if(ch > sh)
                    s.height(ch);
                else
                    c.height(sh);

                if(undefined != FB)
                    FB.Canvas.setSize();
            }

            function checkPaymentDate() {
                var bd = new Date(boletoDate);
                var ed = new Date(eBankingDate);

                var dateToCheck = $("input[name='search_start_date']").first().val();
                if (dateToCheck)
                    var nearDate = reformatDate(dateToCheck);

                $("input[name='search_start_date']").each(function (index, element) {
                    var dt = reformatDate($(element).val());

                    if (nearDate > dt)
                        nearDate = dt;
                });


                var payMethod = $('input[name="payment_method"]:checked').val();
                var boletoRadio = $('input[name="payment_method"][value="boleto"]');
                var ebankingRadio = $('input[name="payment_method"][value="ebanking"]');
                var cardsRadio = $('input[name="payment_method"][value="cards"]');
                var boletoDiv = $('.boleto_wrapper > .boleto');
                var boletoError = boletoDateError + "<br /><strong>Data limite:</strong> " + formatDate(bd, formats.date_format_DP);
                var eBankingDiv = $('.r2 > .ebanking');
                var eBankingError = eBankingDateError + "<br /><strong>Data limite:</strong> " + formatDate(ed, formats.date_format_DP);
                var cardsDiv = $('.r2 > .visa');
                boletoDiv.qtip('destroy');
                eBankingDiv.qtip('destroy');

                if (nearDate <= bd) {
                    boletoRadio.attr('disabled', 'disabled');

                    if (payMethod == "boleto")
                        cardsRadio.click();

                    setQtip(boletoDiv, boletoError);
                    boletoDiv.hide();
                }
                else {
                    boletoRadio.removeAttr('disabled');
                    boletoDiv.qtip('destroy');
                    boletoDiv.show();
                }

                if (nearDate <= ed) {
                    ebankingRadio.attr('disabled', 'disabled');

                    if (payMethod == "ebanking")
                        cardsRadio.click();

                    setQtip(eBankingDiv, eBankingError);
                    eBankingDiv.hide();
                    cardsDiv.show();
                }
                else {
                    ebankingRadio.removeAttr('disabled');
                    eBankingDiv.qtip('destroy');

                    eBankingDiv.show();
                    cardsDiv.show();
                }

                return true;
            }

            $(document).on('change', 'select[name^="adults["]', function(){
                var ad_s = $(this);
                var row = ad_s.closest('tr');
                var kd_s = $('select[name^="kids["]', row);

                var max = row.data('max');
                var kd = kd_s.val();
                var ad = ad_s.val();

                var kdOptions = generate_dropdown_options(0, max-ad, kd);

                kd_s.html(kdOptions);
                animateSegura();
                reCalcSeguraPrice(ad_s);
            });


            $(document).on('change', 'select[name^="kids["]', function(){
                var kd_s = $(this);
                var row = kd_s.closest('tr');
                var ad_s = $('select[name^="adults["]', row);

                var max = row.data('max');
                var kd = kd_s.val();
                var ad = ad_s.val();

                var adOptions = generate_dropdown_options(1, max-kd, ad);

                ad_s.html(adOptions);
                animateSegura();
                reCalcSeguraPrice(kd_s);
            });

            //
            // Form Field Events - END
            //



            //
            // Cart Events and Helpers - START
            //

            $('input[name="payment_method"]').change(function () {
                val = $(this).val();

                if (val == "cards") {
                    $('.cards').show();
                    $('.choose_bank').hide();
                }
                else {
                    $('.cards').hide();
                    if (val == "ebanking") {
                        $('.choose_bank').show();
                    }
                    else {
                        $('.choose_bank').hide();
                    }
                }

                if(undefined != FB)
                    FB.Canvas.setSize();
            });

            $('.finalizea').click(function (e) {
                e.preventDefault();
                var submitButton = $(this);
                var form = $('#reservationDetailsForm');
                startSpinner();
                $.scrollTo(0);
                if (form.valid()) {
                    var bday = $('input[name="birthday_day"]'),
                        bmonth = $('input[name="birthday_month"]'),
                        byear = $('input[name="birthday_year"]'),
                        rday = $('input[name="issue_day"]'),
                        rmonth = $('input[name="issue_month"]'),
                        ryear = $('input[name="issue_year"]');

                    if(bday && bday.is(':visible')){
                        var birthday = bday.val() + '/' +
                            bmonth.val() + '/' +
                            byear.val();

                        var rg_date = rday.val() + '/' +
                            rmonth.val() + '/' +
                            ryear.val();

                        var vb = validDate(birthday);
                        var vr = validDate(rg_date);
                        var valid = (vb.sameDate && vr.sameDate);

                        bday.toggleClass('error', !vb.sameDate);
                        bmonth.toggleClass('error', !vb.sameDate);
                        byear.toggleClass('error', !vb.sameDate);

                        rday.toggleClass('error', !vr.sameDate);
                        rmonth.toggleClass('error', !vr.sameDate);
                        ryear.toggleClass('error', !vr.sameDate);

                        if(!valid){
                            stopSpinner();
                            submitButton.removeAttr('disabled');
                            $('.suas').show();
                            return false;
                        }
                    }

                    $('.suas').hide();

                    $(this).attr('disabled', 'disabled');
                    var post_data = form.serializeArray();
                    post_data.push({name: 'date_format', value: formats.date_format});
                    $.ajax({
                        url: urls.prepare,
                        type: "POST",
                        data: post_data,
                        success:function (data) {
                            try {
                                data = $.parseJSON(data);
                            }
                            catch (err) {
                                stopSpinner();
                                submitButton.removeAttr('disabled');
                                $('.suas').show();
                                alert(err);

                                return;
                            }

                            if (data.success == true) {
                                stopSpinner();
                                submitButton.removeAttr('disabled');
                                FB.Canvas.scrollTo(0, 0);
                                $('input[name="transaction_id"]').val(data.tid);
                                setTimeout(function(){window.location.href = urls.done;}, 50);
                                //window.location.href = urls.done;
                            }else{
                                stopSpinner();
                                submitButton.removeAttr('disabled');
                                $('.suas').show();

                                if (data.message)
                                    alert(data.message);
                                if (data.description)
                                    alert(data.description);
                            }
                        },
                        error:function(){
                            window.location.href="/";
                        }
                    });
                }
                else {
                    stopSpinner();
                    console.debug('invalid');
                    submitButton.removeAttr('disabled');
                    $(this).removeAttr('disabled');
                    FB.Canvas.scrollTo(0, $('.error:first').position().top - 40);
                    $.scrollTo('.error', 200, {offset: {top:-40}});
                    return;
                }
            });

            function setQtip(elem, error) {
                if (error) {
                    var my = 'left center';
                    var at = 'right center';
                    var ad = 0;

                    if (elem.hasClass('ebanking')) {
                        my = 'right center';
                        at = 'left center';
                        ad = 7;
                    }

                    // Apply the tooltip only if it isn't valid
                    elem.filter(':not(.valid)').qtip({
                        overwrite: false,
                        content: error,
                        position: {
                            my: my, /*corners[ flipIt ? 0 : 1 ],*/
                            at: at, /*corners[ flipIt ? 1 : 0 ],*/
                            viewport: false,
                            adjust: {
                                y: ad
                            }
                        },
                        show: {
                            event: 'click',
                            solo: true
                        },
                        hide: false, /*{
                         delay: 5000,
                         when: {
                         event: 'inactive unfocus'
                         }
                         },*/
                        style: {
                            classes: 'qtip-blue' // Make it red... the classic error colour!
                        }
                    })

                        // If we have a tooltip on this element already, just update its content
                        .qtip('option', 'content.text', error);
                }
            }

            $(document).on('focus', 'form input.error, form select.error', function(){
                var elem = $(this);
                var error = elem.attr('error-message');
                if(error){
                    console.debug(error);
                    //setQtip(elem, error);
                }
            });

            $(document).on('blur', 'form input.error, form select.error', function(){
                var elem = $(this);
                elem.qtip('destroy');
            });

            $(document).on('change', 'select[name^="qty_rooms["]', function(){
                var qty_s = $(this);
                var rooms = 0;

                $('select[name^="qty_rooms"]').each(function(index, element){
                    rooms+=$(element).val();
                });

                fluidLayout();

                $('.best_price').toggle(rooms > 0);

                fixRoomsCount($(this));
                animateSegura();
                reCalcSeguraPrice(qty_s);
            });

            $('.secure_notice').click(function(e){
                e.preventDefault();
                _this.showPopup('secure_popup');
            });

            $('.terms_link').click(function(e){
                e.preventDefault();
                _this.showPopup('terms_popup');
            });

            $('select[name="hear_about"]').change(function(){
                if($(this).val() != "Other"){
                    $('.hear_other_how').addClass('hide');
                } else {
                    $('.hear_other_how').removeClass('hide');
                }
            });

            //
            // Cart Events and Helpers - END
            //



            //
            // Currency and Localization Helpers - START
            //

            function fixRoomsCount ($el){
                var _room = $el.closest('tr').data('id')+'',
                    room = _room.split('-'),
                    $rooms_plus_packages = $('tr.room_type_row[data-id="'+room[0]+'"], tr.room_type_row[data-id^="'+room[0]+'-"]', 'table.rooms'),
                    this_position = parseInt($rooms_plus_packages.index($el.closest('tr'))),
                    selected = [],
                    count = 0,
                    total_rooms=0;

                $rooms_plus_packages.each(function(index){
                    if($(this).is('[data-package="0"]'))
                        total_rooms = +$('[name="qty_rooms['+room[0]+']"]', this).data('max_rooms');
                    var i = +($('[name^="qty_rooms['+room[0]+'"]', this).val() || 0);
                    selected.push(i);
                    count += i;

                    if (this_position != index)
                        $('[name^="qty_rooms['+room[0]+'"]>option', this).remove();
                });

                total_rooms -= count;

                $rooms_plus_packages.each(function(index){
                    var $_el = $('[name^="qty_rooms['+room[0]+'"]', this),
                        max_rooms = parseInt($_el.attr('data-max_rooms'));

                    if(total_rooms > 0){
                        max_rooms = selected[index] + total_rooms;
                    }
                    else
                        max_rooms = selected[index];

                    if (this_position != index)
                        $('[name^="qty_rooms['+room[0]+'"]', $(this)).html(generate_dropdown_options(0, max_rooms < 5? max_rooms: 5))
                    $_el.val(selected[index]);
                });
            }

            function getDateComponents (str_date, delimiter)
            {
                delimiter = delimiter || '-'
                var splittedDate = str_date.split(delimiter);
                return [splittedDate[0], parseInt(splittedDate[1]) - 1, splittedDate[2]];
            }

            function reformatDate(dateToCheck) {
                var date = dateToCheck.split("/");
                var day = date[0];
                var month = date[1];
                var year = date[2];

                var inDate = new Date(year + '/' + day + '/' + month);

                if(formats.date_format_DP == 'dd/mm/yy')
                    inDate = new Date(year + '/' + month + '/' + day);

                return inDate;
            }

            function formatDate(date, format) {
                var d = date.getDate();
                var m = date.getMonth() + 1;
                var Y = date.getFullYear();

                if (d < 10)
                    d = "0" + d;
                if (m < 10)
                    m = "0" + m;

                return format.replace('yy', Y).replace('mm', m).replace('dd', d);

            }

            function formatCurrency(str) {
                var tstr = String(str);
                if (tstr.indexOf(formats.currency_symbol) != -1)return str;
                tstr = String(parseFloat(tstr).toFixed(2));
                tstr = tstr.replace(".", formats.mon_decimal_point);

                var negative = false;
                if (tstr.indexOf('-') == 0){
                    negative = true;
                    tstr = tstr.replace('-', '');
                }

                if (tstr.length > 6) {
                    tstr = tstr.split(formats.mon_decimal_point);
                    var begin = tstr[0].length - (parseInt(tstr[0].length / 3) * 3);
                    var newStr = begin == 0 ? '' : tstr[0].substr(0, begin) + formats.mon_thousands_sep;
                    for (var i = begin; i < tstr[0].length; i += 3) {
                        newStr += tstr[0].substr(i, 3) + formats.mon_thousands_sep;
                    }
                    tstr = newStr.substr(0, newStr.length - 1) + formats.mon_decimal_point + tstr[1];
                }
                return (negative? '- ': '')+formats.currency_symbol + " " + tstr;
            }

            function parseCurrency(str) {
                var tstr = String(str);
                if (tstr.indexOf(formats.currency_symbol) == -1) return tstr;
                tstr = tstr.replace(formats.currency_symbol + " ", "");
                tstr = tstr.replace(formats.mon_thousands_sep, '');
                tstr = tstr.replace(formats.mon_decimal_point, '.');
                return parseFloat(parseFloat(tstr).toFixed(2));
            }

            //
            // Currency and Localization Helpers - END
            //



            //
            // Initialize Form Inputs and Page Elements - START
            //

            $('input[name="start_date"]').val(start_date);
            $('input[name="end_date"]').val(end_date);
            $('input[name="adults"]').val(adults);
            $('input[name="kids"]').val(kids);

            $('input[type="text"]').change();
            $('input[type="checkbox"]').change();
            $('input:checked').change();
            $('input[type="submit"]').removeAttr('disabled');

            prepareFormValidation('#searchRoomForm');

            searchAvailRooms();

            FB.init({
                appId      : fb_app,              // App ID from the app dashboard
                status     : true,                // Check Facebook Login status
                xfbml      : true,                // Look for social plugins on the page
                cookie     : true
            });

            setTimeout(function () {
                FB.Canvas.setSize();
            }, 3000);

            fluidLayout();

            $('.pesquisar').focus();

            //
            // Initialize Form Inputs and Page Elements - END
            //

            this.initGallery(fb);

            //
            // Facebook - START
            //

            //
            // Facebook - END
            //



            //
            // Scroll "Reservas" - START
            //

            $(document).scroll(function(){
                var doc = document.documentElement,
                    body = document.body;

                var element = $('.short_reservas');
                var wrapper = $('.form_fields');

                var scroll = (doc && doc.scrollTop  || body && body.scrollTop  || 0);
                var top = wrapper.position().top;
                var left = element.position().left;
                var bottom = wrapper.position().top + wrapper.outerHeight(true);
                var height = element.outerHeight(true);

                if(scroll <= top){
                    element.css('position', 'static').css('top', 'auto');
                }

                if(scroll > top){
                    element.css('position', 'fixed').css('top', 0).css('left', left);
                }

                if(scroll + height > bottom){
                    element.css('position', 'absolute').css('top', bottom-height);
                }
            });

            //
            // Scroll "Reservas" - END
            //
        },

        showPopup:function(className){
            $('.popup').hide();
            $('.popup.'+className).show();
            $('.modal-overlay').show("fade");
        },

        hidePopup:function(){
            $('.modal-overlay').hide("fade", function(){
                $('.popup').hide();
            });
        },
        initGallery: function(fb){
            //
            // Galleria - START
            //

            var image_array = [],
                _this = this;
            fb = fb || false;
            //Galleria.loadTheme('/assets/util/js/themes/classic/galleria.classic.min.js');

            Galleria.on('lightbox_image', function (e) {
                if(fb){
                    FB.Canvas.getPageInfo(
                        function (info) {
                            var cl_h = info.clientHeight;
                            var scroll = info.scrollTop;
                            var $el = $('.galleria-lightbox-box');
                            var m_h = $el.outerHeight(true);
                            var o_h = info.offsetTop;
                            var modal_top = ((cl_h - m_h) / 2) + scroll;

                            $el.css('top', modal_top);
                        }
                    );
                } else {
                    //$.scrollTo('.galleria-lightbox-box', 400);
                }
            });

            $(document).on('click', '.small .room_image, .room_photo div', function (e) {
                e.preventDefault();

                var room = null;
                var details = null;
                var img = $(this);

                if(img.hasClass('room_image')){
                    details = img.closest('.info_about_rooms');
                    room = details.prev();
                } else{
                    room = img.closest('.room_type_row');
                    details = room.next();
                }

                var idx = 0,
                    i = 0;

                var bg = $('.room_photo div', room).css('background-image').substring(4);
                bg = bg.substring(0, bg.length - 1).replace(/"/g, '').replace(/'/g, '').replace('_thumb.', '_featured.');

                var data = [
                    { image: bg }
                ];

                $('.small .room_image', details).each(function (index, element) {
                    i++;
                    var bg = $(element).css('background-image').substring(4);
                    bg = bg.substring(0, bg.length - 1).replace(/"/g, '').replace(/'/g, '').replace('_thumb.', '.');

                    data.push({ image: bg });
                    if (img.css('background-image') == $(element).css('background-image')) {
                        idx = i;
                    }
                });

                Galleria.run('#galleria', {
                    dataSource: data,
                    height: 500,
                    width: 800,
                    lightbox: false,
                    overlayOpacity: 0.7,
                    overlayBackground: '#000'
                });

                _this.showPopup('galleria_popup');

                $('.galleria-thumbnail-nav-left').click(function(){
                    Galleria.get(0).prev();
                });

                $('.galleria-thumbnail-nav-right').click(function(){
                    Galleria.get(0).next();
                });
            });

            $(document).on('click', '.package_terms', function(e){
                e.preventDefault()
                $('.modal-overlay .terms_package_popup .popup-body').html($(this).next().html());
                _this.showPopup('terms_package_popup');
                return false;
            })

            //
            // Galleria - END
            //

            $('#hotel-slider').bxSlider({
                auto: true,
                pause: 5000,
                controls: false,
                slideWidth: 195,
                pagerCustom: '#hotel-slider-pager'
            });
        },
        initMap : function(latitude, longitude, hotel_name){
            var map;
            function _init() {
                var latLng = new google.maps.LatLng(latitude, longitude);
                var mapOptions = {
                    zoom: 15,
                    center: latLng,
                    streetViewControl: false,
                    mapTypeControl: false,
                    zoomControl: false,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };
                map = new google.maps.Map(document.getElementById('map_canvas'),
                    mapOptions);

                var marker = new google.maps.Marker({
                    position: latLng,
                    map: map,
                    title: hotel_name
                });
            }
            google.maps.event.addDomListener(window, 'load', _init);
        }
    };
});
