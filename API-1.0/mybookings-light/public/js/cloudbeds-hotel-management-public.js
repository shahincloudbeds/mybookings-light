

function cloudbedsajax(id, propertyidarg, datefrom, dateto, dateformat, isapi){
    if(isapi == 'yes')
      var dataurl = cb_hm_front_ajaxobj.name+'_api_search';
    else 
      var dataurl = cb_hm_front_ajaxobj.name+'_search'
    var data = {

        action: dataurl,

        id: propertyidarg, 

        datefrom: datefrom ,

        dateto: dateto,

        dateformat: dateformat

    };



     

    jQuery.post(cb_hm_front_ajaxobj.url, data, function(response) {

        jQuery('#'+id).html(response).removeClass('cloudbeds_loading');

    });

} 



jQuery(function($){

  OMBooking = {

    init: function( //Datepicker

      urls,minDate, formats, adults, kids, start_date, end_date,

      //Facebook

      fb_app, 

      // Validation parameters

      boletoDate, eBankingDate,

      // Error Messages

      disabled_msg, no_rooms_msg, not_enough_rooms_msg, no_rooms_selected_msg, boletoDateError, eBankingDateError,

      // Var initialization

      ct, roomCount, cartCount, dep_type, dep_pct, dep_val, taxes, latitude, longitude, hotel_name, fb, country,

      taxes_included, included_taxes_type, terms_tax_percent, terms_tax_fixed, fees_included, fees_included_type,

      fees_percent, fees_fixed,

      // DOM Templates

      newRoom, newPopup, newDetailedRatesPopup, newDetails, newDetailsPackage, newBookingRoom, searchform, is_api

      ){

      $('body').addClass('cb_hm_frontend cb_hm_api');

      var _this = this;

      if(hotel_name !='') {

        requestGetAvailability(searchform);

      }

      formats.date_format_DP = formats.date_format

          .replace('y', 'yy')

          .replace('Y', 'yy')

          .replace('m', 'mm')

          .replace('d', 'dd');

 

      $('.ofdate').datepicker({     

          defaultDate: "+0",     

          dateFormat: formats.date_format_DP,

          minDate: "+0",

          onSelect: function (dateText, inst) {

            var d = $.datepicker.parseDate(inst.settings.dateFormat, dateText);

            d.setDate(d.getDate() + 1);

            if($(this).is('input[name="search_start_date"]')){

              $('input[name="search_end_date"]').val($.datepicker.formatDate(formats.date_format_DP, d));

            }



         },

         onClose: function (selectedDate) {

            if($(this).is('input[name="search_start_date"]')){

                var dateMin = $('input[name="search_start_date"]').datepicker("getDate");

                var newDate = new Date(dateMin.getFullYear(), dateMin.getMonth(),dateMin.getDate() + 1);

                $('input[name="search_end_date"]').datepicker("option", "minDate", newDate);

                $('input[name="search_end_date"]').datepicker('refresh').datepicker('show');

            }

        }

      });

      

      $(document).on('click','.cloudbedssearch',function(e){

        e.preventDefault();

        var form = jQuery(this).closest('form'),

            start_date = $('input[name="search_start_date"]').val(),

            end_date = $('input[name="search_end_date"]').val();

        requestGetAvailability(form);

      });



      function requestGetAvailability(form) {

        var form = jQuery(form);

        jQuery('#roomsavailability').html('<div class="room_container"><div class="cloudbeds_loading"></div></div>');



        if(is_api == 'yes'){
          var extraurl = cb_hm_front_ajaxobj.name+'_api_checkroomavailability'
        }else {
          var extraurl = cb_hm_front_ajaxobj.name+'_checkroomavailability'
        }

         var extradata = {

                   'action' : extraurl

            };

 

        jQuery.ajax({

          url: cb_hm_front_ajaxobj.url,

          data: form.serialize() + '&' + $.param(extradata), 

          method: 'post',

          beforeSend: function(){

            //console.log('Checking Rooms Availability');

          },

          success:function(response){

               jQuery('#roomsavailability').html(response);

               initializerooms();

          },

          error: function(){

              jQuery('#roomsavailability').html('Error ! Checking Rooms Availability Try Again.');

          }

        });

        return false;

      }



      jQuery(document).on('change', 'select[name^="adults["]', function(){

          var ad_s = jQuery(this);

          var row = ad_s.closest('tr');

          var kd_s = jQuery('select[name^="kids["]', row);



          var max = row.data('max');

          var kd = kd_s.val();

          var ad = ad_s.val();



          var kdOptions = generate_dropdown_options(0, max-ad, kd);



          kd_s.html(kdOptions);

          animateSegura();

          initializerooms();

          reCalcSeguraPrice(ad_s);

      });



      $(document).on("mouseover mouseout", ".room_name_container", function(e){

        if(e.type == 'mouseover'){

          $('.stay', this).removeClass('omhide');

        }else {

          $('.stay', this).addClass('omhide');

        }

      });



      $(document).on("mouseover mouseout", ".avg_rate_container", function(e){

        if(e.type == 'mouseover'){

          $('.stay.detailed_rates', this).removeClass('omhide');

        }else {

          $('.stay.detailed_rates', this).addClass('omhide');

        }

      });



      $(document).on('click', '.name_room', function (e) {

          e.preventDefault();

          var nr = $(this);

          var open = nr.hasClass('for_minus');

          var row = nr.closest('.room_type_row');



          $('.name_room').removeClass('for_minus').addClass('for_plus');



          row.siblings('.info_about_rooms').find('.room_details_container').hide();

          row.siblings('.info_about_rooms').hide();



          if(!open){

              var tabs = $('.tab_info2 > div', row.next('.info_about_rooms'));



              var m_h = 0;



              tabs.each(function(index, element){

                  var clone = $(element).clone();



                  clone.css({

                      'opacity': 0,

                      'position': 'absolute',

                      'top': 0,

                      'max-width': 769

                  })

                  .appendTo('body');



                  var tmp_height = clone.height();

                  if(tmp_height > m_h)

                      m_h = tmp_height;

                  clone.remove();

              });



              //tabs.css('height', m_h);



              nr.removeClass('for_plus').addClass('for_minus');

              row.next('.info_about_rooms').show();



              row.next('.info_about_rooms').find('.room_details_container').show();

          }

      });



      $(document).on('click', 'ul.room_type_tabs li', function(){

          var details = $(this).closest('.info_about_rooms');

          var li = $('ul.room_type_tabs li', details);

          var idx = $(this).index();

          var max = li.length-1;

          if(idx > 0 && idx < max){

              idx--;

              li.removeClass('active');

              $(this).addClass('active');

              $('.tab_info2 > div', details).removeClass('active');

              $('.tab_info2 > div:eq('+idx+')', details).addClass('active');

          }

      });

      $(document).on('change', 'select[name^="qty_rooms["]', function(){

          var qty_s = $(this);

          var rooms = 0;



          $('select[name^="qty_rooms"]').each(function(index, element){

              rooms+=$(element).val();

          });



          $('.best_price').toggle(rooms > 0);



          fixRoomsCount($(this));

          animateSegura();

          reCalcSeguraPrice(qty_s);

      });



      $(document).on('click', '.book_now',function () {

          var qty = 0;

          var total = 0;

          var dep = 0;

          var total_tax = 0;



          var sub_tax = 0;

          var sub_fees = 0;



          var form = $("#reservationDetailsForm");

          var checkin = $('input[name="search_start_date"]').val();

          var checkout = $('input[name="search_end_date"]').val();

          var rm_cont = $('.selected_room_container');



          $('input[name$="selected_"]', form).remove();

          $('.short_info2', rm_cont).remove();



          $("select[name^='qty_rooms[']").each(function(index, element){

              var selected = $(element).val();


              if(selected > 0){

                  var row = $(element).closest('.room_type_row');

                  var id = row.data('id');

                  var adults = $('select[name^="adults["]', row).val();

                  var kids = $('select[name^="kids["]', row).val();

                  var val = parseCurrency(row.data('val'));

                  var adt = row.data('adults');

                  var kid = row.data('kids');

                  var add_adt = parseCurrency(row.data('add-adults'));

                  var add_kid = parseCurrency(row.data('add-kids'));

                  var name = row.data('name');



                  var  room = val;



                  var fixes_js_rounding = 0.0001;


                  for(var i = 0; i < selected; i++){

                      if (adults > adt){

                          room += add_adt * (adults - adt);

                      }

                      if (kids > kid){

                          room += add_kid * (kids - kid);

                      }



                      var room_tax = 0;

                      var room_dep = 0;



                      if(dep_type == 'percent'){



                          if (taxes_included == 'Y' && included_taxes_type == 'percent')

                              room_tax += parseFloat(parseFloat(dep_pct *  terms_tax_percent * room + fixes_js_rounding).toFixed(2));



                          if (fees_included == 'Y' && fees_included_type == 'percent')

                              room_tax += parseFloat(parseFloat(dep_pct * fees_percent * room + fixes_js_rounding).toFixed(2));



                          room_dep = parseFloat(dep_pct * room + fixes_js_rounding).toFixed(2);

                      } else {

                          var nights = $('.nights_count').text();

                          room_dep = (room < (dep_val*nights))?room:(dep_val*nights);

                      }



                      dep += parseFloat(room_tax) + parseFloat(room_dep);

                      total += parseFloat(room);

                  }
                      // console.log(total);



                  if(dep_type == 'percent'){

                      var tmp_tax = 0;

                      if (taxes_included == 'Y' && included_taxes_type == 'fixed')

                          tmp_tax += terms_tax_fixed;

                      if (fees_included == 'Y' && fees_included_type == 'fixed')

                          tmp_tax += fees_fixed;



                      dep += parseFloat(parseFloat(dep_pct * tmp_tax + fixes_js_rounding).toFixed(2));

                  }





                  if (taxes_included == 'Y')

                      sub_tax =  included_taxes_type == 'percent'? total * terms_tax_percent : terms_tax_fixed;



                  if (fees_included == 'Y')

                      sub_fees =  fees_included_type == 'percent'? total * fees_percent : fees_fixed;



                  total_tax = parseFloat(sub_tax.toFixed(2)) + parseFloat(sub_fees.toFixed(2));



                  qty+=selected;

                  var rm_p = $('.clonable_for_selected_room_container').clone().removeClass('omhide clonable_for_selected_room_container');

                  $('.selected_room_container_name', rm_p).html(name);

                  $('.selected_room_container_selected_count', rm_p).html(selected);

                  $('.selected_room_container_adults', rm_p).html(adults);

                  $('.selected_room_container_kids', rm_p).html(kids);

                  rm_cont.append(rm_p);

                  form.append('<input type="hidden" name="selected_room_qty['+id+']" value="'+selected+'" />');

                  form.append('<input type="hidden" name="selected_adults['+id+']" value="'+adults+'" />');

                  form.append('<input type="hidden" name="selected_kids['+id+']" value="'+kids+'" />');

                  form.append('<input type="hidden" name="selected_room_name['+id+']" value="'+name+'" />');

                  form.append('<input type="hidden" name="selected_room_total['+id+']" value="'+room+'" />');

              }

          });



          if(qty==0){

              alert(no_rooms_selected_msg);

              return;

          }



          var days = calculateDays(checkin, checkout);



          $('.checkin_date').text(checkin);

          $('.checkout_date').text(checkout);

          $('.nights_int').text(days);
          // console.log(days);
          $('.total_value').text(formatCurrency(total));



          if (taxes_included == 'Y')

              $('.sub_tax').text(formatCurrency(sub_tax));



          if (fees_included == 'Y')

              $('.sub_fees').text(formatCurrency(sub_fees));

          else

              $('.sub_fees_row').remove();





          prepareFormValidation($('#reservationDetailsForm'));

          checkPaymentDate();



          $('.total_taxes').text(formatCurrency(total_tax));

          // $('.total_deposit').text(formatCurrency(dep));
          $('.total_deposit').text('0');

          // $('.total_checkin').text(formatCurrency(total + total_tax - dep));
          $('.total_checkin').text(formatCurrency(total + total_tax));

          $('.grand_total').text(formatCurrency(total + total_tax));



          form.append('<input type="hidden" name="selected_checkin" value="'+checkin+'" />');

          form.append('<input type="hidden" name="selected_checkout" value="'+checkout+'" />');



          form.append('<input type="hidden" name="total_advance" value="'+(dep)+'" />');

          form.append('<input type="hidden" name="grand_total" value="'+(total + total_tax)+'" />');



          country_selector_changes();



          of_hide($('.chooser'), function(){

              of_hide($('.search_panel'), function(){

                  of_show($('.suas'), function(){
                      $(this).attr('height','auto');
                      $("#cb_hm_progress").find('.cb_hm_c:eq(0)').removeClass('active').addClass('done');
                      $("#cb_hm_progress").find('.cb_hm_c:eq(1)').addClass('active');
                      $('.change_reserve').slideDown(300);

                      $("#reservationDetailsForm").validate({

                        ajaxSubmit: function(form){

                            

                        }

                      })

                  })

              });

          });

      });





      $(document).on('change','.country_selector', function(e){ 

              country_selector_changes();

      });



       

      $(document).on('click','.finalizecentral',function(e){

        e.preventDefault();

        var v = $(this),

            form = v.closest('form');

         if(form.valid() ){ 

            var extradata = {

                   'action' : cb_hm_front_ajaxobj.name+'_reservation' 

            };



            $.ajax({

              url: cb_hm_front_ajaxobj.url,

              type: "POST",

              beforeSend: function(){

                v.attr('disabled','disabled');

                $("#roomsavailability").append('<span class="ajaxloader" />');

                $("html, body").stop().animate({scrollTop:$("#roomsavailability").position().top}, 500, 'swing');

              },

              data: form.serialize() + '&' + $.param(extradata),

              success:function (response) {

                  $("#roomsavailability").find('.ajaxloader').remove();

                  v.removeAttr('disabled');

                  

                   if(response.indexOf('--ERROR--') != -1){ 
                      $("#cb_hm_progress").find('.cb_hm_c').removeClass('active').addClass('active');
                      $("#reservationresponse").html(response);

                    }else{ 

                      $("#roomsavailability").html(response);

                    }

                    

              },

              error:function(response){

                  $("#roomsavailability").find('.ajaxloader').remove();

                  v.removeAttr('disabled');

              }

            });

          }else {

            $("#reservationDetailsForm").find('.error:eq(0)').focus();

          }

      });







      $('select[name="hear_about"]').change(function(){

          if($(this).val() != "Other"){

              $('.hear_other_how').addClass('omhide');

          } else {

              $('.hear_other_how').removeClass('omhide');

          }

      });



      jQuery.validator.addMethod("cvv", function(cvvCode) {

          if(!ct)

              return true;



          var digits = 0;

          switch (ct) {

              case 'amex':

                  digits = 4;

                  break;

              default:

                  digits = 3;

                  break;

          }



          var regExp = new RegExp('[0-9]{' + digits + '}');

          return (cvvCode.length == digits && regExp.test(cvvCode));



      }, "CVV invalid, please check the data reported");



      jQuery.validator.addMethod("cc", function(value) {

          value = value.replace(/-/g, '');

          value = value.replace(/ /g, '');

          value = value.replace(/\./g, '');



          var __indexOf = [].indexOf || function(item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; };



          var card, card_type, card_types, get_card_type, is_valid_length, is_valid_luhn, normalize, validate, validate_number;

          card_types = [

              {

                  name: 'amex',

                  pattern: /^3[47]/,

                  valid_length: [15]

              }, {

                  name: 'diners_club_carte_blanche',

                  pattern: /^30[0-5]/,

                  valid_length: [14]

              }, {

                  name: 'diners_club_international',

                  pattern: /^36/,

                  valid_length: [14]

              }, {

                  name: 'jcb',

                  pattern: /^35(2[89]|[3-8][0-9])/,

                  valid_length: [16]

              }, {

                  name: 'laser',

                  pattern: /^(6304|670[69]|6771)/,

                  valid_length: [16, 17, 18, 19]

              }, {

                  name: 'visa_electron',

                  pattern: /^(4026|417500|4508|4844|491(3|7))/,

                  valid_length: [16]

              }, {

                  name: 'visa',

                  pattern: /^4/,

                  valid_length: [16]

              }, {

                  name: 'mastercard',

                  pattern: /^5[1-5]/,

                  valid_length: [16]

              }, {

                  name: 'maestro',

                  pattern: /^(5018|5020|5038|6304|6759|676[1-3])/,

                  valid_length: [12, 13, 14, 15, 16, 17, 18, 19]

              }, {

                  name: 'discover',

                  pattern: /^(6011|622(12[6-9]|1[3-9][0-9]|[2-8][0-9]{2}|9[0-1][0-9]|92[0-5]|64[4-9])|65)/,

                  valid_length: [16]

              }

          ];



          get_card_type = function(number) {

              var _j, _len1, _ref2;

              _ref2 = (function() {

                  var _k, _len1, _results;

                  _results = [];

                  for (_k = 0, _len1 = card_types.length; _k < _len1; _k++) {

                      card = card_types[_k];



                      _results.push(card);

                  }

                  return _results;

              })();

              for (_j = 0, _len1 = _ref2.length; _j < _len1; _j++) {

                  card_type = _ref2[_j];

                  if (number.match(card_type.pattern)) {

                      return card_type;

                  }

              }

              return null;

          };



          is_valid_luhn = function(number) {

              var digit, n, sum, _j, _len1, _ref2;

              sum = 0;

              _ref2 = number.split('').reverse();

              for (n = _j = 0, _len1 = _ref2.length; _j < _len1; n = ++_j) {

                  digit = _ref2[n];

                  digit = +digit;

                  if (n % 2) {

                      digit *= 2;

                      if (digit < 10) {

                          sum += digit;

                      } else {

                          sum += digit - 9;

                      }

                  } else {

                      sum += digit;

                  }

              }

              return sum % 10 === 0;

          };



          is_valid_length = function(number, card_type) {

              var _ref2;

              return _ref2 = number.length, __indexOf.call(card_type.valid_length, _ref2) >= 0;

          };



          validate_number = function(number) {

              var length_valid, luhn_valid;

              card_type = get_card_type(number);

              luhn_valid = false;

              length_valid = false;

              if (card_type != null) {

                  luhn_valid = is_valid_luhn(number);

                  length_valid = is_valid_length(number, card_type);

              }



              if(!card_type || !luhn_valid || !length_valid)

                  return false;



              ct = card_type['name'];

              return true;

          };



          validate = function() {

              var number = value.replace(/[ .-]/g, '');

              return validate_number(number);

          };



          normalize = function(number) {

              return ;

          };



          return validate();

      }, "Card not valid, please check the data reported");



      jQuery.validator.addMethod("ccdate", function() {

          return validCCDate();

      }, "Invalid date, please check the data reported");



      $.validator.addMethod("cpf", function(value, element) {

            if(value.length == 0)

                return true;



            value = jQuery.trim(value);



            value = value.replace('.','');

            value = value.replace('.','');

            cpf = value.replace('-','');

            while(cpf.length < 11) cpf = "0"+ cpf;

            var expReg = /^0+$|^1+$|^2+$|^3+$|^4+$|^5+$|^6+$|^7+$|^8+$|^9+$/;

            var a = [];

            var b = new Number;

            var c = 11;

            for (i=0; i<11; i++){

                a[i] = cpf.charAt(i);

                if (i < 9) b += (a[i] * --c);

            }

            if ((x = b % 11) < 2) { a[9] = 0 } else { a[9] = 11-x }

            b = 0;

            c = 11;

            for (y=0; y<10; y++) b += (a[y] * c--);

            if ((x = b % 11) < 2) { a[10] = 0; } else { a[10] = 11-x; }



            var retorno = true;

            if ((cpf.charAt(9) != a[9]) || (cpf.charAt(10) != a[10]) || cpf.match(expReg)) retorno = false;



            return this.optional(element) || retorno;



        }, "Invalid date, please check the data reported");

      

      $.validator.addMethod("cep", function(cep, element){

          if(cep.length == 0)

              return true;



          cep = jQuery.trim(cep);



          cep = cep.replace(' ','');

          cep = cep.replace('.','');

          cep = cep.replace('-','');



          if(cep.length != 8)

              return false;



          var str = '' + parseInt(cep, 10);

          while (str.length < 8) {

              str = '0' + str;

          }



          if(str == cep)

              return true;



          return false;

      }, "Invalid date, please check the data reported");



      this.initGallery(false);



      function prepareFormValidation(form) {

          var f = $(form).get(0);

          $.removeData(f, 'validator');

          $(form).validate({

              errorPlacement: function (error, element) { //hide error messages

                var elem = $(element);

                if(!error.is(':empty')) {

                  elem.attr('error-message', error.html());

                  // setQtip(element, error.html());

                  error.appendTo(element.parent());

                }else {

                    elem.qtip('destroy');

                    elem.removeAttr('error-message');

                }

              },

              success: $.noop, // Odd workaround for errorPlacement not firing!

              rules: {

                  cpf: {

                      cpf: true

                  },

                  zip: {

                      cep: true

                  },

                  search_start_date: {

                      ofdate: true

                  },

                  card_number: {

                      cc: true

                  },

                  cvv: {

                      cvv: true

                  },

                  exp_month: {

                      ccdate: true

                  },

                  exp_year: {

                      ccdate: true

                  }

              }

          });

      }



      function initializerooms(){

        //jQuery('#roomsavailability select').selectize('destroy');

        //jQuery('#roomsavailability select').selectize();

        jQuery('.cloudbeds_container .info_about_rooms').hide();

        _this.initGallery(false);

      }



      function generate_dropdown_options(start, max, selected) {

          var options = '';



          var first = start;

          var min = start;

          if (isNaN(start)) {

              min = 0;

          }



          for (var i_a = min; i_a <= max; i_a++) {

              options += '<option value="';



              if (i_a == min && isNaN(start))

                  options += '"';

              else

                  options += i_a + '"';



              if (selected && i_a == selected)

                  options += ' selected="selected"';



              options += '>';



              if (i_a == min)

                  options += start;

              else

                  options += i_a;



              options += '</option>';



          }

          return options;

      }



      function setQtip(elem, error) {

          if (error) {

              var my = 'left center';

              var at = 'right center';

              var ad = 0;



              if (elem.hasClass('ebanking')) {

                  my = 'right center';

                  at = 'left center';

                  ad = 7;

              }



              // Apply the tooltip only if it isn't valid

              elem.filter(':not(.valid)').qtip({

                  overwrite: false,

                  content: error,

                  position: {

                      my: my, /*corners[ flipIt ? 0 : 1 ],*/

                      at: at, /*corners[ flipIt ? 1 : 0 ],*/

                      viewport: false,

                      adjust: {

                          y: ad

                      }

                  },

                  show: {

                      event: 'click',

                      solo: true

                  },

                  hide: false, /*{

                   delay: 5000,

                   when: {

                   event: 'inactive unfocus'

                   }

                   },*/

                  style: {

                      classes: 'qtip-blue' // Make it red... the classic error colour!

                  }

              })



                  // If we have a tooltip on this element already, just update its content

                  .qtip('option', 'content.text', error);

          }

      }



      $(document).on('focus', 'form input.error, form select.error', function(){

          var elem = $(this);

          var error = elem.attr('error-message');

          if(error){

              // console.debug(error);

              // setQtip(elem, error);

          }

      });



      function of_hide(element, callback){

          // element.hide({

          //     effect: 'slide',

          //     easing: 'swing',

          //     duration: 'slow',

          //     complete: function(){

          //         if(undefined != callback)

          //             callback();

          //     }

          // });
          element.stop().slideUp('slow', function(){
            if(undefined != callback)
              callback();
          });



          if(undefined != callback)

              callback();

      }



      function of_show(element, callback){

          // element.show({

          //     effect: 'blind',

          //     easing: 'swing',

          //     duration: 'slow',

          //     complete: function(){

          //         if(undefined != callback)

          //             callback();

          //     }

          // });
          element.stop().slideDown('slow', function(){
            if(undefined != callback)
              callback();
          });



          if(undefined != callback)

              callback();

      }



      function fixRoomsCount ($el){

          var _room = $el.closest('tr').data('id')+'',

              room = _room.split('-'),

              $rooms_plus_packages = $('tr.room_type_row[data-id="'+room[0]+'"], tr.room_type_row[data-id^="'+room[0]+'-"]', 'table.rooms'),

              this_position = parseInt($rooms_plus_packages.index($el.closest('tr'))),

              selected = [],

              count = 0,

              total_rooms=0;



          $rooms_plus_packages.each(function(index){

              if($(this).is('[data-package="0"]'))

                  total_rooms = +$('[name="qty_rooms['+room[0]+']"]', this).data('max_rooms');

              var i = +($('[name^="qty_rooms['+room[0]+'"]', this).val() || 0);

              selected.push(i);

              count += i;



              if (this_position != index)

                  $('[name^="qty_rooms['+room[0]+'"]>option', this).remove();

          });



          total_rooms -= count;



          $rooms_plus_packages.each(function(index){

              var $_el = $('[name^="qty_rooms['+room[0]+'"]', this),

                  max_rooms = parseInt($_el.attr('data-max_rooms'));



              if(total_rooms > 0){

                  max_rooms = selected[index] + total_rooms;

              }

              else

                  max_rooms = selected[index];



              if (this_position != index)

                  $('[name^="qty_rooms['+room[0]+'"]', $(this)).html(generate_dropdown_options(0, max_rooms < 5? max_rooms: 5))

              $_el.val(selected[index]);

          });

      }



      function animateSegura() {

          var seguraColors = ['#c6e1ff', '#white'];

          for (var i = 0; i < 7; i++) {

              $('.segura').animate({

                  backgroundColor: seguraColors[i % 2]

              }, 300 );

          }

      }



      function calculateDays(checkin, checkout){

          var sd = validDate(checkin),

              sd_standard = sd.standardDate;



          sd = sd.convertedDate;



          var ed = validDate(checkout),

              ed_standard = ed.standardDate;



          ed = ed.convertedDate;



          var ed_standard_split = getDateComponents(ed_standard, '/'),

              ed_standard_date = (new Date(ed_standard_split[0], ed_standard_split[1], ed_standard_split[2])).getTime(),

              sd_standard_split = getDateComponents(sd_standard, '/'),

              sd_standard_date = (new Date(sd_standard_split[0], sd_standard_split[1], sd_standard_split[2])).getTime();



          return parseInt((ed_standard_date-sd_standard_date)/(1000*60*60*24));

      }



      function reCalcSeguraPrice(node) {

          // var row = node.closest('tr');

          // var qty_s = $('select[name^="qty_rooms["]', row);

          //        if (qty_s.val() > 0) {

          //

          //        } else {

          //            $('.segura .compra, .segura .compra_price, .segura .compra_notice').text('');

          //        }

          var qty = 0;

          var total = 0;

          var dep = 0;

          var tax = 0;

          var roomCount = 0;



          var form = $("#reservationDetailsForm");

          var checkin = $('input[name="search_start_date"]').val();

          var checkout = $('input[name="search_end_date"]').val();

          var rm_cont = $('.selected_room_container');



          $('input[name$="selected_"]', form).remove();

          $('.short_info2', rm_cont).remove();



          $("select[name^='qty_rooms[']").each(function(index, element){

              var selected = $(element).val();

              roomCount += parseInt(selected);

              // console.log(selected);

              if(selected > 0){

                  var row = $(element).closest('.room_type_row');

                  var id = row.data('id');

                  var adults = $('select[name^="adults["]', row).val();

                  var kids = $('select[name^="kids["]', row).val();

                  var val = parseCurrency(row.data('val'));

                  var adt = row.data('adults');

                  var kid = row.data('kids');

                  var add_adt = parseCurrency(row.data('add-adults'));

                  var add_kid = parseCurrency(row.data('add-kids'));

                  var name = row.data('name');



                  var room = val;



                  for(var i = 0; i < selected; i++){

                      if (adults > adt){

                          room += add_adt * (adults - adt);

                      }

                      if (kids > kid){

                          room += add_kid * (kids - kid);

                      }



                      var fixes_js_rounding = 0.0001;



                      var room_tax = 0;

                      var room_dep = 0;

                      if(dep_type == 'percent'){

                          room_tax = parseFloat(dep_pct * taxes * room + fixes_js_rounding).toFixed(2);

                          room_dep = parseFloat(dep_pct * room + fixes_js_rounding).toFixed(2);

                      } else {

                          var nights = $('.nights_count').text();

                          room_dep = (room < (dep_val*nights)) ? room : (dep_val*nights);

                      }

                      // console.log(room, room_tax, taxes, room_dep, total);

                      dep += parseFloat(room_tax) + parseFloat(room_dep);

                      tax += taxes * room;

                      total += parseFloat(room);
                      // console.log(total);
                  }

                  // console.log(total);

                  qty+=selected;

                  var rm_p = '<p class="short_info2">' +

                      name +

                      ' (x' + selected + ') <br />'+

                      ' <span class="bold">Adultos:</span>' + adults + ', '+

                      ' <span class="bold">Children:</span>' + kids +

                      '</p>';

                  rm_cont.append(rm_p);



                  form.append('<input type="hidden" name="selected_room_qty['+id+']" value="'+selected+'" />');

                  form.append('<input type="hidden" name="selected_adults['+id+']" value="'+adults+'" />');

                  form.append('<input type="hidden" name="selected_kids['+id+']" value="'+kids+'" />');

                  // get rate ids
                  jQuery.each(row.find('.roomrateid'), function(){
                    var v = jQuery(this);
                    form.append('<input type="hidden" name="'+v.attr('name')+'" value="'+v.attr('value')+'" />');
                  })

                  // form.append('')

              }

          });



          if (total > 0) {

              $('.segura .compra_rooms span.roomsCount').text(roomCount);

              $('.segura .compra_price').text(formatCurrency(total));

              $('.segura .compra_notice').removeClass('omhide');

              $('.segura .compra_rooms').removeClass('omhide');

              $('.segura .compra').removeClass('omhide').addClass('omhide');

          } else {

              resetSeguraBlock();

          }

      }



      function resetSeguraBlock(){

          $('.segura .compra_price').text('');

          $('.segura .compra_notice').removeClass('omhide').addClass('omhide');

          $('.segura .compra_rooms').removeClass('omhide').addClass('omhide');

          $('.segura .compra').removeClass('omhide');

      }



      function reFromateTable(){

        //$selectize.selectize('refreshItems');

      }



      function formatCurrency(str) {

          var tstr = String(str);

          if (tstr.indexOf(formats.currency_symbol) != -1)return str;

          tstr = String(parseFloat(tstr).toFixed(2));

          tstr = tstr.replace(".", formats.mon_decimal_point);



          var negative = false;

          if (tstr.indexOf('-') == 0){

              negative = true;

              tstr = tstr.replace('-', '');

          }



          if (tstr.length > 6) {

              tstr = tstr.split(formats.mon_decimal_point);

              var begin = tstr[0].length - (parseInt(tstr[0].length / 3) * 3);

              var newStr = begin == 0 ? '' : tstr[0].substr(0, begin) + formats.mon_thousands_sep;

              for (var i = begin; i < tstr[0].length; i += 3) {

                  newStr += tstr[0].substr(i, 3) + formats.mon_thousands_sep;

              }

              tstr = newStr.substr(0, newStr.length - 1) + formats.mon_decimal_point + tstr[1];

          }
          if(formats.currency_position == 'before') {
            return (negative? '- ': '')+formats.currency_symbol + " " + tstr;
          }else {
            return (negative? '- ': '')+tstr+""+formats.currency_symbol;
          }

      }



      function parseCurrency(str) {

          var tstr = String(str);

          if (tstr.indexOf(formats.currency_symbol) == -1) return tstr;

          tstr = tstr.replace(formats.currency_symbol + " ", "");

          tstr = tstr.replace(formats.mon_thousands_sep, '');

          tstr = tstr.replace(formats.mon_decimal_point, '.');

          return parseFloat(parseFloat(tstr).toFixed(2));

      }



      function validDate(value){

          var shortDateFormat = formats.date_format_DP;

          var res = true;

          var arr = value.split('/');

          try {

              if(arr[2] < 100 && arr[2] >=13){

                  arr[2] = "20"+arr[2];

                  value = arr.join("/");

              }



              $.datepicker.parseDate(shortDateFormat, value);

          } catch (error) {

              if(arr.length == 2){

                  arr[2] = (new Date()).getFullYear();

                  value = arr.join("/");



                  try{

                      $.datepicker.parseDate(shortDateFormat, value);

                  }

                  catch(error){

                      res = false;

                  }

              } else {

                  res = false;

              }

          }



          if(res){

              var today = new Date((new Date()).getFullYear(), (new Date()).getMonth(), (new Date()).getDate());

              var toCompare = new Date(arr[2], arr[0]-1, arr[1]);



              if(formats.date_format_DP == 'dd/mm/yy')

                  toCompare = new Date(arr[2], arr[1]-1, arr[0]);



              if(toCompare < today){

                  res = false;

              }

          }



          var array = {}, d = null;

          array.result = res;

          array.convertedDate = value;



          if(shortDateFormat == 'dd/mm/yy'){

              array.standardDate = arr[2]+'/'+arr[1]+'/'+arr[0];

              d = new Date(arr[2], arr[1] - 1, arr[0]);

              array.sameDate = (d && (d.getMonth() + 1) == arr[1] && d.getDate() == Number(arr[0]));

          }

          else{

              array.standardDate = arr[2]+'/'+arr[0]+'/'+arr[1];

              d = new Date(arr[2], arr[0] - 1, arr[1]);

              array.sameDate = (d && (d.getMonth() + 1) == arr[0] && d.getDate() == Number(arr[1]));

          }



          return array;

      }



      function validCCDate(){

                var y_input = $('select[name="exp_year"]');

                var m_input = $('select[name="exp_month"]');

                var year = y_input.val();

                var month = m_input.val();



                var dt = new Date(year, month-1, 1, 0, 0, 0);

                var td = new Date();



                if(dt > td){

                    y_input.parent().removeClass('error').addClass('valid');

                    m_input.parent().removeClass('error').addClass('valid');

                } else {

                    y_input.parent().removeClass('valid').addClass('error');

                    m_input.parent().removeClass('valid').addClass('error');

                }



                return (dt > td);

            }



      function getDateComponents (str_date, delimiter)

      {

          delimiter = delimiter || '-'

          var splittedDate = str_date.split(delimiter);

          return [splittedDate[0], parseInt(splittedDate[1]) - 1, splittedDate[2]];

      }



      function reformatDate(dateToCheck) {

          var date = dateToCheck.split("/");

          var day = date[0];

          var month = date[1];

          var year = date[2];



          var inDate = new Date(year + '/' + day + '/' + month);



          if(formats.date_format_DP == 'dd/mm/yy')

              inDate = new Date(year + '/' + month + '/' + day);



          return inDate;

      }



      function formatDate(date, format) {

          var d = date.getDate();

          var m = date.getMonth() + 1;

          var Y = date.getFullYear();



          if (d < 10)

              d = "0" + d;

          if (m < 10)

              m = "0" + m;



          return format.replace('yy', Y).replace('mm', m).replace('dd', d);



      }

      function checkPaymentDate() {

          var bd = new Date(boletoDate);

          var ed = new Date(eBankingDate);



          var dateToCheck = $("input[name='search_start_date']").first().val();

          if (dateToCheck)

              var nearDate = reformatDate(dateToCheck);



          $("input[name='search_start_date']").each(function (index, element) {

              var dt = reformatDate($(element).val());



              if (nearDate > dt)

                  nearDate = dt;

          });





          var payMethod = $('input[name="payment_method"]:checked').val();

          var boletoRadio = $('input[name="payment_method"][value="boleto"]');

          var ebankingRadio = $('input[name="payment_method"][value="ebanking"]');

          var cardsRadio = $('input[name="payment_method"][value="cards"]');

          var boletoDiv = $('.boleto_wrapper > .boleto');

          var boletoError = boletoDateError + "<br /><strong>Data limite:</strong> " + formatDate(bd, formats.date_format_DP);

          var eBankingDiv = $('.r2 > .ebanking');

          var eBankingError = eBankingDateError + "<br /><strong>Data limite:</strong> " + formatDate(ed, formats.date_format_DP);

          var cardsDiv = $('.r2 > .visa');

          boletoDiv.qtip('destroy');

          eBankingDiv.qtip('destroy');



          if (nearDate <= bd) {

              boletoRadio.attr('disabled', 'disabled');



              if (payMethod == "boleto")

                  cardsRadio.click();



              setQtip(boletoDiv, boletoError);

              boletoDiv.hide();

          }

          else {

              boletoRadio.removeAttr('disabled');

              boletoDiv.qtip('destroy');

              boletoDiv.show();

          }



          if (nearDate <= ed) {

              ebankingRadio.attr('disabled', 'disabled');



              if (payMethod == "ebanking")

                  cardsRadio.click();



              setQtip(eBankingDiv, eBankingError);

              eBankingDiv.hide();

              cardsDiv.show();

          }

          else {

              ebankingRadio.removeAttr('disabled');

              eBankingDiv.qtip('destroy');



              eBankingDiv.show();

              cardsDiv.show();

          }



          return true;

      }



      function checkPaymentDate() {

          var bd = new Date(boletoDate);

          var ed = new Date(eBankingDate);



          var dateToCheck = $("input[name='search_start_date']").first().val();

          if (dateToCheck)

              var nearDate = reformatDate(dateToCheck);



          $("input[name='search_start_date']").each(function (index, element) {

              var dt = reformatDate($(element).val());



              if (nearDate > dt)

                  nearDate = dt;

          });





          var payMethod = $('input[name="payment_method"]:checked').val();

          var boletoRadio = $('input[name="payment_method"][value="boleto"]');

          var ebankingRadio = $('input[name="payment_method"][value="ebanking"]');

          var cardsRadio = $('input[name="payment_method"][value="cards"]');

          var boletoDiv = $('.boleto_wrapper > .boleto');

          var boletoError = boletoDateError + "<br /><strong>Data limite:</strong> " + formatDate(bd, formats.date_format_DP);

          var eBankingDiv = $('.r2 > .ebanking');

          var eBankingError = eBankingDateError + "<br /><strong>Data limite:</strong> " + formatDate(ed, formats.date_format_DP);

          var cardsDiv = $('.r2 > .visa');

          boletoDiv.qtip('destroy');

          eBankingDiv.qtip('destroy');



          if (nearDate <= bd) {

              boletoRadio.attr('disabled', 'disabled');



              if (payMethod == "boleto")

                  cardsRadio.click();



              setQtip(boletoDiv, boletoError);

              boletoDiv.hide();

          }

          else {

              boletoRadio.removeAttr('disabled');

              boletoDiv.qtip('destroy');

              boletoDiv.show();

          }



          if (nearDate <= ed) {

              ebankingRadio.attr('disabled', 'disabled');



              if (payMethod == "ebanking")

                  cardsRadio.click();



              setQtip(eBankingDiv, eBankingError);

              eBankingDiv.hide();

              cardsDiv.show();

          }

          else {

              ebankingRadio.removeAttr('disabled');

              eBankingDiv.qtip('destroy');



              eBankingDiv.show();

              cardsDiv.show();

          }



          return true;

      }



    },



    showPopup:function(className){

        $('.popup').hide();

        $('.popup.'+className).show();

        $('.modal-overlay').show("fade");

    },



    hidePopup:function(){

        $('.modal-overlay').hide("fade", function(){

            $('.popup').hide();

        });

    },

    initGallery: function(fb){

        jQuery('.tabGallery').each(function() { // the containers for all your galleries

            jQuery(this).magnificPopup({

                delegate: 'a', // the selector for gallery item

                type: 'image',

                gallery: {

                  enabled:true

                }

            });

        }); 

        $(document).on('click', '.room_photo div', function (e) {

            e.preventDefault();

            jQuery(this).closest('tr').next('.info_about_rooms').find('.tabGallery a:eq(0)').trigger('click');

        });

    },

  }

});





WebFontConfig = {

    google: { families: [ 'Roboto:400,100,100italic,300,400italic,700,700italic:latin' ] }

  };

(function() {

    var wf = document.createElement('script');

    wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +

      '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';

    wf.type = 'text/javascript';

    wf.async = 'true';

    var s = document.getElementsByTagName('script')[0];

    s.parentNode.insertBefore(wf, s);

})();

 





function country_selector_changes(){

  if(jQuery('.country_selector').val() == 'BR'){

      jQuery('div.data').show();

      jQuery('div.cpp').show();

      jQuery('div.sexo').show();



      jQuery('div.telefone').show();

      jQuery('div.telefone input').addClass('required');

      jQuery('div.celular').show();

      jQuery('div.celular input').addClass('required');



      jQuery('div.hear_about').show();



      jQuery('div.rua').show();

      jQuery('div.rua input').addClass('required');

      jQuery('div.numero').show();

      jQuery('div.numero input').addClass('required');

      jQuery('div.complemento').show();



      jQuery('div.bairro').show();

      jQuery('div.bairro input').addClass('required');

      jQuery('div.cidade').show();

      jQuery('div.cidade input').addClass('required');

      jQuery('div.estado').show();

      jQuery('div.cep').show();



      jQuery('hr.hide_line').show();



      jQuery('div.rg').show();

      jQuery('div.data_emissao').show();



      jQuery('div.orgao').show();

      jQuery('div.estade_emissao').show();



      jQuery('div.radio_block').show();



      jQuery('div.zip_foreign').hide();

  } else {

      jQuery('div.data').hide();

      jQuery('div.data.de').show(); // Expiration Date



      jQuery('div.cpp').hide();

      //jQuery('div.sexo').hide();



      //jQuery('div.telefone').hide();

      jQuery('div.telefone input').removeClass('required error');

      //jQuery('div.celular').hide();

      jQuery('div.celular input').removeClass('required error');



      //jQuery('div.hear_about').hide();

      //jQuery('div.hear_other_how').hide();



      //jQuery('div.rua').hide();

      jQuery('div.rua input').removeClass('required error');

      //jQuery('div.numero').hide();

      jQuery('div.numero input').removeClass('required error');

      //jQuery('div.complemento').hide();



      //jQuery('div.bairro').hide();

      jQuery('div.bairro input').removeClass('required error');

      //jQuery('div.cidade').hide();

      jQuery('div.cidade input').removeClass('required error');

      jQuery('div.estado').hide();

      jQuery('div.cep').hide();



      jQuery('hr.hide_line').hide();



      jQuery('div.rg').hide();

      jQuery('div.data_emissao').hide();



      jQuery('div.orgao').hide();

      jQuery('div.estade_emissao').hide();



      jQuery('input[name="payment_method"][value="cards"]').click();

      jQuery('div.radio_block').hide();



      jQuery('div.zip_foreign').show();

      jQuery('a.userAgreementLink').click(function(e){
          e.preventDefault();

          jQuery('.black_popup_overlay').show();
          jQuery('.terms_popup').show();
      });

      jQuery('img.close_popup').click(function(e){
          e.preventDefault();

          jQuery('.black_popup_overlay').hide();
          jQuery('.terms_popup').hide();
      });
  }

}

