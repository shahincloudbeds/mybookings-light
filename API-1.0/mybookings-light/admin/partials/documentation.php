<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://www.omasters.com
 * @since      0.0.1
 *
 * @package    Cloudbeds_Hotel_Management
 * @subpackage Cloudbeds_Hotel_Management/admin/partials
 * @template   Documentation
 */
?>
<div class="cb_hm wrap">
	<div class="clearfix">
		<div class="mixin-col span_12 text-center">
			<img src="<?php echo $plugin_admin; ?>images/cloudbeds.png" alt="Cloudbeds" />
			<p>Cheers to more reservations and happier guests.</p>
		</div>
	</div>
	<div class="container_panel documentation_panel">	
		<h1>Cloudbeds WP mybookings plugin - beta version</h1>
		<div class="clearfix">
			<h3>Description</h3>
			<p>The <strong>Cloudbeds WP mybookings plugin</strong> is the Wordpress version of the world class Online Booking Engine from <a href="https://www.cloudbeds.com" target="_blank">Cloudbeds</a>, making it easier to install and more customizable than ever! Hoteliers than run a WP-based website can now have a free, easy to use booking engine. Cloudbeds customers can benefit from full integration with the <a href="https://www.clodubeds.com/myfrontdesk" target="_blank">myfrontdesk</a> property management system, a cloud-based PMS.</p>

			<h3>Instructions</h3>
			<p>After installing the plugin, you must select between one of two connectivity modes: <strong>Premium - Use Cloudbeds API</strong> or <strong>Free - Use System Management</strong>.</p>
			<p>If you want the <strong>Free - Use System Management</strong> mode, please select "Use System Management" under the plugin settings page. This will enable the settings tabs where you will need to fill out all details about your property, amenities, accommodations, policies and rates. On this mode, you will need to visit the "Reservations" page to check if you have received new reservations through your online booking engine.</p>
			<p>After that, place the shortcode <code>[cb_hm date_format="d/m/Y"]</code> on the body of the page you’ll use as your booking page.</p>
			<p>However, if you’re a <a href="https://www.cloudbeds.com" target="_blank">Cloudbeds</a> customer and know the benefits of a fully integrated cloud-based solution, the WP mybookings plugin will allow you to:
			<ul>
				<li>Integrate your pre-defined booking engine into your website, always showing your own domain name in the browser’s address bar</li>
				<li>Customize the CSS code of your page to make it look just like your website</li>
				<li>Receive new reservations directly into your myfrontdesk PMS, including all guest and reservation details, ready for room allocation and ultrafast check-in</li>
				<li>Collect deposits upon reservation, offering multiple payment methods</li>
				<li>Manage all your inventory from a central pool, maximizing your revenue while reducing the risk of overbookings.</li>
			</ul>
			<p>To use the <strong>Premium - Use Cloudbeds API</strong> Mode, fill out the "API Key" field with the code provided to you by our customer coaches.</p>
			<p>Then apply the shortcode <code>[cb_hm property_id="000"]</code> to the body of the page you’ll use as your booking page, replacing ‘000’ with your Property ID #, provided to you by our customer coaches. Publish the page and test it out!</p>
			<p>Notice that the plugin will pull availability and rates from your Cloudbeds’ account, and send the reservations received through your booking page directly into your system pages.</p>

			<h3>Plugin Customization</h3>
			<p>In order to customize the looks of your booking page, you will need (at least basic) knowledge about Wordpress, HTML and CSS. You will also need Admin access to Wordpress (most possible, if you’re reading this) and also FTP access to your website’s web folders.</p>
			<p>To customize the CSS, you will need to have your custom file under the folder: /wp-content/themes/YOURTHEMEFOLDERNAME/mybookings-light/with the file name mybookings_light.css </p>
			<p>where ‘YOURTHEMEFOLDERNAME’ is the name of the folder of your website’s active theme.</p>
			<p>You can get the plugin’s default CSS file (and use it as a baseline) from the folder /wp-content/plugins/mybookings-light/public/css/cloudbeds-hotel-management-public.css Make a copy of it, adjust CSS as desired, and place it in the custom folder.</p>
			<p>You can also customize:
			Search Form: search.php file
			Thank you Page: thankyou.php
			and Booking Details Page: jstemplate.php (we recommend backing up this file, and also testing after any changes to this file. If anything is wrong on this, the plugin will crash).</p>

			<h3>Disclaimer</h3>
			<p>The Cloudbeds plugin is under Beta test phase. No support is provided to <strong>Free - Use System Management</strong> Mode users. Cloudbeds customers can report any issues through the customer service contact points.</p>
			<p>Some features available in the original mybookings module are still under development for the WP mybookings plugin.</p>
			<p>WP mybookings user is responsible for the purchase and installation of a SSL security certificate if wishes to reinforce the security of the transactions and (possibly) increase conversions - something we strongly recommend.</p>
			<br/>
		</div>
	</div>
</div>
