(function($) {

    var cbedsRates, defaultOptions, __bind, __this;

    __bind = function(fn, me) {
        return function() {
            return fn.apply(me, arguments);
        };
    };

    // Plugin default options.
    defaultOptions = {
        dateFormat: 'dd-mm-yy',
        messagetemplate: '<div class="cb_hm_ajax_message_container"> {{#if status}} <span class="cb_hm_{{status}}"></span> {{/if}} {{message}} </div>',
        resulttemplate: '<thead> <tr class="monthhead"> <th colspan="2"></th> {{#each months}} <th class="month" colspan="{{colspan}}">{{title}}</th> {{/each}} </tr> <tr class=""> <th colspan="2"> <a href="javascript:void(0)" class="calendar-prev btn btn-warning"><i class="fa fa-chevron-left"></i></a> <a href="javascript:void(0)" class="calendar-next btn btn-warning"><i class="fa fa-chevron-right"></i></a> </th> {{#each days}} <th>{{title}}, {{value}}</th> {{/each}} </tr> </thead> <tbody> {{#each roomrate}} <tr> <td class="roomname">{{title}}</td> <td class="roomrate"> <label>Rooms</label> <label>Rate(rooms)</label> </td> {{#each rates}} <td> <input type="text" class="form-control" name="rooms[{{../roomid}}][{{day}}]" placeholder="0" value="{{available}}" /> <input type="text" class="form-control" name="rates[{{../roomid}}][{{day}}]" placeholder="0" value="{{price}}" /> </td> {{/each}} </tr> {{/each}} </tbody>',
        url:'admin-ajax.php',
        action: 'getrates',
        dateFrom: '01-03-2015',
        dateTo: '10-03-2015',
        toolpanel: '.ratepanel',
        name: null
    };

    cbedsRates = (function(options) {

        function cbedsRates(handler, options) {
            this.handler = handler;
            // plugin variables.
            this.resultdata = {};
            this.formChange = false;
            this.ajaxrequest = false;

            // Extend default options.
            $.extend(true, this, defaultOptions, options);

            // $.extend(true, resultdata, {result:'something'});

            // Bind methods.
            this.update = __bind(this.update, this);
            this.onResize = __bind(this.onResize, this);
            this.init = __bind(this.init, this);
            this.clear = __bind(this.clear, this);
            __this = this;

            // Listen to resize event if requested.
            if (this.autoResize) {
                $(window).bind('resize.cbedsRates', this.onResize);
            };
            jQuery('.pickdate').datepicker({
                showOn: 'button',
                defaultDate: "+0",
                dateFormat: defaultOptions.dateFormat,
                onSelect: function(dateText, inst) {
                    __this.toDate(dateText);
                },
                onClose: function(selectedDate) {
                }
            }).datepicker('widget').wrap('<div class="cb_hm_frontend"/>');

            // datepicker between two dates to set longterm
            jQuery("#datefrom").datepicker({
              inline: true,
              showOtherMonths: true,
              changeMonth: true,
              changeYear: true,
              dateFormat: 'dd-mm-yy',
              onClose: function(dateText, inst) { 
              },
              onSelect: function (selected) {
                jQuery("#dateto").datepicker("option", "minDate", selected);
              }
            }).datepicker('widget').wrap('<div class="cb_hm_frontend"/>');
            
            jQuery("#dateto").datepicker({
              inline: true,
              showOtherMonths: true,
              changeMonth: true,
              changeYear: true,
              dateFormat: 'dd-mm-yy',
            }).datepicker('widget').wrap('<div class="cb_hm_frontend"/>');
          
            jQuery('.saverateChanges').on('click', this, this.saveRates);
            // jQuery('.setlongTerm').on('click', function(e){
            //   e.preventDefault();
            //   jQuery("#longtermScreen").modal('show');
            // });
            jQuery('.saveLongTerm').on('click', this, this.saveRates);
            jQuery('.selectInLabel').on('change', function(){
              if(jQuery(this).val() == 'close'){
                jQuery(this).closest('.form-group').find('input[name="units"]').stop().hide();
                jQuery(this).closest('.form-group').find('input[name="close"]').stop().show();
              }else{
                jQuery(this).closest('.form-group').find('input[name="units"]').stop().show();
                jQuery(this).closest('.form-group').find('input[name="close"]').stop().hide();
              }
            });
            // console.log(this);
        };

        // Method for updating the plugins options.
        cbedsRates.prototype.update = function(options) {
            $.extend(true, this, options);
        };

        // This timer ensures that layout is not continuously called as window is being dragged.
        cbedsRates.prototype.onResize = function() {
        };

        // Example API function.
        cbedsRates.prototype.resizeFunc = function() {
            console.log('window resized, table needed to update');
        };

        // Main method.
        cbedsRates.prototype.init = function() {
            // jQuery(document).on('click','.ofdate', function(e){
            //   jQuery('#datepicker').show();
            // });
            this.getResult();

        };

        cbedsRates.prototype.getResult = function(){
          if(__this.formChange){
            if(confirm('Data modified, do you want to continue?')){
              __this.getRates();
            }
          }else {
             __this.getRates();
          }
        };

        cbedsRates.prototype.getRates = function(){
          var data = {
            action: this.action,
            from: this.dateFrom,
            to: this.dateTo
          }
          if(!__this.ajaxrequest){
            __this.formChange = false;

            jQuery.ajax({
              type:'GET',
              url: this.url,
              data: data,
              beforeSend: function(){
                __this.loadingmsg();
              },
              success: function(json){__this.ajaxrequest = false;
                jQuery('.cb_hm_ajax_message').stop().remove();
                if(json.success){
                  __this.resultdata = json.data;
                  __this.render();
                }else {
                  if(json == '-1'){
                    jQuery("body").find('.cb_hm_ajaxloader').html("<div class='alert msgsboard alert-danger'>Something is wrong with form submission, please reload the page</div>");
                  }else {
                    var template = Handlebars.compile(__this.messagetemplate);
                    jQuery('.cb_hm_ajax_message').html(template(data));
                  }
                  __this.show_cb_hm_message();
                  window.setTimeout(function() {jQuery('.cb_hm_ajax_message').stop().animate({top:'-200px'}, 400, function(){jQuery("body").find('.cb_hm_ajax_message').stop().remove();});}, 2000);
                }
              },
              error: function(){
                alert('error');
                jQuery("body").find('.cb_hm_ajaxloader').stop().remove();
              }
            });
          }
        }

        cbedsRates.prototype.render = function() {
          // console.log('render');
          jQuery(__this.toolpanel).css({opacity:1});
          var resultdata = __this.resultdata;
          var template = Handlebars.compile(__this.resulttemplate);
          jQuery("#rate-table").html(template(resultdata));
          jQuery('.calendar-prev').on('click', this.previous);
          jQuery('.calendar-next').on('click', this.next);
          jQuery('table.rate-table input.form-control[type="text"]').on('change', function(){
            jQuery(this).addClass('modified');
            __this.formChange = true;
          });  
        };

        cbedsRates.prototype.next = function(){
          var datefrom = __this.dateTo;
          var dateto = moment(datefrom, 'D-M-YYYY').add(10, 'days').format('D-M-YYYY');
          options = {
            dateFrom: datefrom,
            dateTo: dateto
          };
          __this.update(options);
          __this.getResult();
        };

        cbedsRates.prototype.previous = function(){
          var dateto = __this.dateFrom;
          var datefrom = moment(dateto, 'D-M-YYYY').subtract('days', 10).format('D-M-YYYY');
          options = {
            dateFrom: datefrom,
            dateTo: dateto
          };
          __this.update(options);
          __this.getResult();
        };

        cbedsRates.prototype.toDate = function(ndate){
          var date = ndate;
          var dnewdate = moment(date, 'D-M-YYYY');
          var from = moment(__this.dateFrom, 'D-M-YYYY');
          var to = moment(__this.DateTo, 'D-M-YYYY');
          if(dnewdate.diff(from, 'days') > 0 && to.diff(dnewdate, 'days') > 0){
            return;
          }else {
            options = {
              dateFrom: dnewdate.subtract(5, 'days').format('D-M-YYYY'),
              dateTo: dnewdate.add(10, 'days').format('D-M-YYYY')
            };
            __this.update(options);
            __this.getResult();
          }
        };

        cbedsRates.prototype.saveRates = function(e){
          e.preventDefault();
          // if(jQuery("#longtermScreen").is(":visible")){
          //   jQuery("#longtermScreen").modal('hide');
          // }
          form = jQuery(this).closest('form');
          formaction = form.find('input[name="action"]').val();
          switch(formaction){
            case __this.name+'_save_rate_changes':
              if(!__this.formChange){
                var data = {
                  'status': 'warning',
                  'message': 'Nothing Changes'
                };
                jQuery('body').append('<div class="cb_hm_ajax_message" />');
                var template = Handlebars.compile(__this.messagetemplate);
                jQuery('.cb_hm_ajax_message').html(template(data));
                __this.show_cb_hm_message();
                window.setTimeout(function() {jQuery('.cb_hm_ajax_message').stop().animate({top:'-200px'}, 400, function(){jQuery("body").find('.cb_hm_ajax_message').stop().remove();});}, 2000);
                return false;
              }
              form.find('input[name="from"]').val(__this.dateFrom);
              form.find('input[name="to"]').val(__this.dateTo);
              data = __this.pullChanges(form);
              break;
            case __this.name+'_save_bulk_changes':
              form.find('input[name="setfor"]').val(jQuery(this).data('set'));
            default:
              data = form.serialize();
              break;
          }
          if(!__this.ajaxrequest){
            $.ajax({
              url: __this.url,
              type: 'POST',
              dataType: 'json',
              data: data,
              beforeSend: function(){
                __this.loadingmsg();
              },
              success: function(json){
                __this.ajaxrequest = false;
                if(json.success){
                  switch(formaction){
                    case __this.name+'_save_bulk_changes':
                      data = json.data;
                      var options = {
                        dateFrom: data.from,
                        dateTo: data.to,
                      }
                      __this.update(options);
                      __this.getResult();
                      break;
                    case __this.name+'_save_rate_changes':
                      form.find('input.modified').removeClass('modified');
                      __this.formChange = false;
                      var template = Handlebars.compile(__this.messagetemplate);
                      jQuery('.cb_hm_ajax_message').html(template(json));
                      __this.show_cb_hm_message();
                      break;
                    default:
                      var template = Handlebars.compile(__this.messagetemplate);
                      jQuery('.cb_hm_ajax_message').html(template(json));
                      __this.show_cb_hm_message();
                      break;
                  }
                  window.setTimeout(function() {jQuery('.cb_hm_ajax_message').stop().animate({top:'-200px'}, 400, function(){jQuery("body").find('.cb_hm_ajax_message').stop().remove();});}, 2000);
                  // __this.resultdata = json.data;
                  // __this.render();
                }else {
                  if(json == '-1'){
                    jQuery("body").find('.cb_hm_ajaxloader').html("<div class='alert msgsboard alert-danger'>Something is wrong with form submission, please reload the page</div>");
                  }else {
                    var template = Handlebars.compile(__this.messagetemplate);
                    jQuery('.cb_hm_ajax_message').html(template(data));
                  }
                  __this.show_cb_hm_message();
                  window.setTimeout(function() {jQuery('.cb_hm_ajax_message').stop().animate({top:'-200px'}, 400, function(){jQuery("body").find('.cb_hm_ajax_message').stop().remove();});}, 2000);
                }
              },
              error: function(){
                alert('error');
                jQuery("body").find('.cb_hm_ajaxloader').stop().remove();
              }
            })
            .done(function() {
              console.log("success");
            })
            .fail(function() {
              console.log("error");
              jQuery("body").find('.cb_hm_ajaxloader').stop().remove();
            })
            .always(function() {
              console.log("complete");
            });
          }

        };

        cbedsRates.prototype.pullChanges = function(form){
          // console.log(form.serialize());
          var data = form.find('input[type="hidden"]').serialize();
          data += '&'+form.find('input.modified').serialize();
          return data;
        }

        // Clear event listeners and time outs.
        cbedsRates.prototype.clear = function() {
            $(window).unbind('resize.cbedsRates', this.onResize);
        };

        cbedsRates.prototype.show_cb_hm_message = function() {
          jQuery(__this.toolpanel).css({opacity:1});
            var wintop = (jQuery(window).height() - jQuery('.cb_hm_ajax_message_container').outerHeight()) / 2;
            var winleft = (jQuery(window).width() - jQuery('.cb_hm_ajax_message_container').outerWidth() - 25) / 2;
            jQuery('.cb_hm_ajax_message_container').css({
                left: winleft
            });
            jQuery('.cb_hm_ajax_message_container').stop().animate({
                top: wintop
            }, 300);
        };

        cbedsRates.prototype.loadingmsg = function(){
          __this.ajaxrequest = true;
          var data = {
            'status': 'loading',
            'message': 'Please wait...'
          };
          jQuery('body').append('<div class="cb_hm_ajax_message" />');
          var template = Handlebars.compile(__this.messagetemplate);
          jQuery('.cb_hm_ajax_message').html(template(data));
          __this.show_cb_hm_message();
          jQuery(__this.toolpanel).css({opacity:0.5});
        }

        return cbedsRates;
    })();

    $.fn.cbedsRates = function(options) {
        // Create a cbedsRates instance if not available.
        if (!this.cbedsRatesInstance) {
            this.cbedsRatesInstance = new cbedsRates(this, options || {});
        } else {
            this.cbedsRatesInstance.update(options || {});
        }

        // Init plugin.
        this.cbedsRatesInstance.init();

        // Display items (if hidden) and return jQuery object to maintain chainability.
        return this.show();
    };

    jQuery(function() {
        if (jQuery("#rate-table").length) {
            var fromdate = moment().format('D-M-YYYY');
            // var fromdate = moment().subtract('days', 5).format('D-M-YYYY');
            var todate = moment().add(10, 'days').format('D-M-YYYY');
            jQuery("#rate-table").cbedsRates({
              action:cb_hm_ajaxobj_rates.name+'_get_rates',
              url:cb_hm_ajaxobj_rates.url,
              dateFrom: fromdate,
              dateTo: todate,
              name: cb_hm_ajaxobj_rates.name,
            });
        }
    });
})(jQuery);
