(function( $ ){
  'use strict';
  var defaultOptions = {};
  var CBHMroomrates = (function(){
    function CBHMroomrates(update, options){
      this.update = update;
      $.extend(true, this, defaultOptions, options);
    };

    CBHMroomrates.prototype.update = function(options){
      $.extend(true, this, options);
    };
    
    CBHMroomrates.prototype.init = function(){
      
      jQuery("#cb-hm-roomrate-update-cell").on('click' ,'.selectall' ,function (event) {
          event.preventDefault();
          var name = jQuery(this).data('type');
          jQuery('input[name="'+name+'"]').prop('checked', true);
      });

      jQuery("#cb-hm-roomrate-update-cell").on('click' ,'.selectnone' ,function (event) {
          event.preventDefault();
          var name = jQuery(this).data('type');
          jQuery('input[name="'+name+'"]').prop('checked', false);
      });

      jQuery("#cb-hm-roomrate-update-cell").on('click', '.calltable', function(e){
        e.preventDefault();
        jQuery.ajax({
          url:jQuery(this).attr('href'),
          success:function(){
            alert('done');
          }
        });
      });
      jQuery('#cb-hm-roomrate-update-cell .datepicker').each(function(){
        var options = {dateFormat : 'yy-mm-dd'};
        if(jQuery(this).data('min-date') !=''){
          options.minDate = jQuery(this).data('min-date');
        }
        if(jQuery(this).data('max-date') !=''){
          options.maxDate = jQuery(this).data('max-date');
        }
        jQuery(this).datepicker(options);
      });
      if(jQuery('#cb-hm-roomrate-update-cell .btooltip').length){
        jQuery("#cb-hm-roomrate-update-cell .btooltip").tooltip();
      }
    };

    return CBHMroomrates;
  })();

  $.fn.CBHMroomrates = function(options) {
    // Create a cbedsHM instance if not available.
    if (!this.CBHMroomInstance) {
      this.CBHMroomInstance = new CBHMroomrates(this, options || {});
    } else {
      this.CBHMroomInstance.update(options || {});
    }
    // Init plugin.
    this.CBHMroomInstance.init();
 
    // Display items (if hidden) and return jQuery object to maintain chainability.
    return this.show();
  };

  window.CBHMroomtosell = window.CBHMroomtosell || {};

  CBHMroomtosell.Inventory = (function (){

      function _flatten(object, levels, values) {
          var result = [];
          values = values || {};
          if (!levels.length) {
            return [ jQuery.extend({ value: object }, values) ];
          }

          var level = levels[0];
          for (var key in object) {
              values[level] = key;
              result = result.concat(
                  _flatten(object[key], levels.slice(1), values));
              delete values[level];
          }
          return result;
      }

      function _vivificate(obj) {
          var keys = Array.prototype.slice.call(arguments, 1);
          for (var i=0; i < keys.length; i++) {
              var key = keys[i];
              obj[key] = obj[key] || {};
              obj = obj[key];
          }
          return obj;
      };

      var Inventory = function (data) {
          this.data = data || {};
      };

      Inventory.prototype._vivificate = function () {
          var obj = this.data;
          for (var i=0; i < arguments.length; i++) {
              var key = arguments[i];
              obj[key] = obj[key] || {};
              obj = obj[key];
          }
          return obj;
      };

      Inventory.prototype.update = function (options) {
          var field = options.field;
          var value = options.value;
          var room  = options.room;
          var rate  = options.rate;
          var occ   = options.occupancy;
          var date  = options.date;

          if (room && rate && occ && date) {
              this._vivificate('hotel_room_rate_occupancy_date',
                               room, rate, occ, date)[field] = value;
          }
          else if (room && rate && date) {
              this._vivificate('hotel_room_rate_date',
                               room, rate, date)[field] = value;
          }
          else if (room && date) {
              this._vivificate('hotel_room_date',
                               room, date)[field] = value;
          }
          else if (date) {
              this._vivificate('hotel_date', date)[field] = value;
          }
          else {
              throw "Invalid arguments for inventory";
          }
          return this;
      };

      Inventory.prototype.flat_fields = function () {
          return [].concat(
              _flatten(
                  this.data['hotel_room_date'] || {},
                  ['room', 'date', 'field'])
          );
      };
      return Inventory;
  })();

  jQuery(document).on('click','.ac-container input.ac-input',function(e){
      jQuery(".ac-container input.ac-input").not(jQuery(this)).removeAttr('checked');
  });

  jQuery(function($){
    if(jQuery("#cb-hm-roomrate-update-cell").length){
      jQuery("#cb-hm-roomrate-update-cell").CBHMroomrates();
    }
    if(jQuery('#cb-hm-roomrate-update-cell #rooms-to-sell-grid').length){
      var updateInventoryDOM = function (data) {
        var inventory = new CBHMroomtosell.Inventory(data);
        var updatedFields = inventory.flat_fields();
        $('input.rooms-to-sell').each(function() {
            var element = $(this);
            var fieldInfo = JSON.parse(element.attr('data-field-info'));
            element.val(fieldInfo.value);
            $.each(updatedFields, function (i, updatedField) {
                if (fieldInfo.room  == updatedField.room &&
                    fieldInfo.date  == updatedField.date &&
                    fieldInfo.field == updatedField.field) {

                    element.val(updatedField.value);

                    element.attr('data-field-info',
                               JSON.stringify(updatedField));
                    element.css({backgroundColor: '#FFFFDD'})
                }
            });
        });
      };

      var updatedInputs = [];
      $('#cb-hm-roomrate-update-cell .matrix-table').on('change', 'input.rooms-to-sell', function () {
          updatedInputs.push($(this));
      })

      $('#cb-hm-roomrate-update-cell #rooms-to-sell-grid').on('click', '.submitGrid', function (event) {
          event.preventDefault();
          $('input.rooms-to-sell').prop('disabled', true);
          var inventory = new CBHMroomtosell.Inventory();

          $.each(updatedInputs, function (i, element) {
              var fieldInfo = JSON.parse(element.attr('data-field-info'));
              fieldInfo.value = element.val();
              inventory.update(fieldInfo);
          });
          updatedInputs = [];

          if (!$.isEmptyObject(inventory.data)) {
            $.ajax({
              type: 'POST',
              url: cb_hm_ajaxobj_rates.url,
              dataType:'json',
              data: {
                  action: cb_hm_ajaxobj_rates.name+'_update-grid',
                  inventory_data: inventory.data
              }
            }).done(function (result) {
                updateInventoryDOM(result.updated_fields);
                if(!result.success){
                  form.prepend('<div class="msgsboard alert alert-danger" />');
                  jQuery.each(json.error, function(i,element){
                    form.find('.msgsboard').append(element+'<br/>');
                  });
                }
                // $.each(['warnings', 'errors', 'notdones'],
                //         function (i, notification) {

                //     if (result[notification].length > 0) {
                //         var element = $('.matrix-' + notification)
                //                        .show().find('ul');
                //         $.each(result[notification], function (i, message) {
                //             element.append($('<li></li>').text(message));
                //         });
                //     }
                // });
                $('input.rooms-to-sell').prop('disabled', false);
            }).fail(function () {
                alert('Inventory update failed');
            });
          }
          else {
              $('input.rooms-to-sell').prop('disabled', false);
          }
      });
    }
    if(jQuery('#cb-hm-roomrate-update-cell #rooms-to-rate-grid').length){
      var updateInventoryDOM = function (data) {
        var inventory = new CBHMroomtosell.Inventory(data);
        var updatedFields = inventory.flat_fields();
        $('input.rooms-to-sell').each(function() {
            var element = $(this);
            var fieldInfo = JSON.parse(element.attr('data-field-info'));
            element.val(fieldInfo.value);
            $.each(updatedFields, function (i, updatedField) {
                if (fieldInfo.room  == updatedField.room &&
                    fieldInfo.date  == updatedField.date &&
                    fieldInfo.field == updatedField.field) {

                    element.val(updatedField.value);

                    element.attr('data-field-info',
                               JSON.stringify(updatedField));
                    element.css({backgroundColor: '#FFFFDD'})
                }
            });
        });
      };

      var updatedInputs = [];
      $('.matrix-table').on('change', 'input.rooms-to-sell', function () {
          updatedInputs.push($(this));
      })

      $('#rooms-to-rate-grid').on('click', '.submitGrid', function (event) {
          event.preventDefault();
          $('input.rooms-to-sell').prop('disabled', true);
          var inventory = new CBHMroomtosell.Inventory();

          $.each(updatedInputs, function (i, element) {
              var fieldInfo = JSON.parse(element.attr('data-field-info'));
              fieldInfo.value = element.val();
              inventory.update(fieldInfo);
          });
          updatedInputs = [];

          if (!$.isEmptyObject(inventory.data)) {
            $.ajax({
              type: 'POST',
              url: cb_hm_ajaxobj_rates.url,
              dataType:'json',
              data: {
                  action: cb_hm_ajaxobj_rates.name+'_update-rate-grid',
                  inventory_data: inventory.data
              }
            }).done(function (result) {
                updateInventoryDOM(result.updated_fields);
                if(!result.success){
                  form.prepend('<div class="msgsboard alert alert-danger" />');
                  jQuery.each(json.error, function(i,element){
                    form.find('.msgsboard').append(element+'<br/>');
                  });
                }
                // $.each(['warnings', 'errors', 'notdones'],
                //         function (i, notification) {

                //     if (result[notification].length > 0) {
                //         var element = $('.matrix-' + notification)
                //                        .show().find('ul');
                //         $.each(result[notification], function (i, message) {
                //             element.append($('<li></li>').text(message));
                //         });
                //     }
                // });
                $('input.rooms-to-sell').prop('disabled', false);
            }).fail(function () {
                alert('Inventory update failed');
            });
          }
          else {
              $('input.rooms-to-sell').prop('disabled', false);
          }
      });
    }
    jQuery('#cb-hm-roomrate-update-cell .matrix-table').on('mouseenter mouseleave', '.status', function(e) {
      if(e.type == 'mouseenter'){
        var tooltip = jQuery(this).find('.status-tooltip');
        if (tooltip.text()) {
            tooltip.show();
        }
      }else {
        jQuery(this).find('.status-tooltip').hide();
      }
    });
    
  });

  jQuery(document).on('click','#booking-rate-table td.status-clickable',function(e){
    e.preventDefault();console.log('adsf');
    var v = jQuery(this),
        updatedField = JSON.parse(v.attr('data-status-info'));
    if(!v.hasClass('disabled')){
      jQuery.ajax({
        url:cb_hm_ajaxobj_rates.url,
        type:'post',
        data:{
          action: cb_hm_ajaxobj_rates.name+'_roomrate-update',
          data:updatedField
        },
        dataType: 'json',
        beforeSend: function(){
          v.addClass('disabled').stop().append('<span class="loading"/>');
        },
        fail: function(){
          v.removeClass('disabled').stop().find('span.loading').stop().remove();
          alert('update-faild');
        },
        success: function(json){
          v.removeClass('disabled').stop().find('span.loading').stop().remove();
          if(json.success){
            dataid = v.data('id');
            console.log(jQuery("#booking-rate-table td.info-"+dataid).length);
            if(json.status == 'bookable'){
              jQuery("#booking-rate-table td.info-"+dataid).removeClass('status-bookable').stop().addClass('status-closed');
            }else {
              jQuery("#booking-rate-table td.info-"+dataid).removeClass('status-closed').stop().addClass('status-bookable');
            }
            jQuery("#booking-rate-table td.info-"+dataid).attr('data-status-info', JSON.stringify(json.datastatusinfo));
          }else {
            alert(json.error);
          }
        }
      });
    }
  });

  jQuery(document).on('change','#cb-hm-roomrate-update-cell input.rooms-to-sell',function(e){
    var v = jQuery(this);
    // console.log(v.attr('min'));
    if(typeof(v.attr('min')) !== 'undefined'){
      var min = parseInt(v.attr('min'));
      if(min > parseInt(v.val()) ){
        alert('You can not set lower then Allocated, as those are already soldout.');
        v.val(min);
      }
    }
  });

  jQuery(document).on('click','#cb-hm-roomrate-update-cell a.status-soldout',function(e){
    e.preventDefault();
    var v = jQuery(this);
    // var data = {
    //   'action': 'updatecommonsell',
    //   'from'  : jQuery('#wizard-date-from').val(),
    //   'until' : jQuery("#wizard-date-to").val(),
    //   'update-period' : 'Select period'
    // }
    var html = '<form id="hiddenForm" method="post" action="'+v.attr('href')+'"><input type="hidden" name="action" value="updatecommonsell" /><input type="hidden" name="from" value="'+jQuery('#wizard-date-from').val()+'" /><input type="hidden" name="until" value="'+jQuery("#wizard-date-until").val()+'" /><input type="hidden" name="update-period" value="Select period" />';
    jQuery('body').append(html);
    jQuery('body').find("#hiddenForm").stop().submit();
  });
})( jQuery );
