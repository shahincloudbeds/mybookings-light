(function( $ ) {
	'use strict';
	var fileInput = '';

	/**
	 * All of the code for your admin-specific JavaScript source
	 * should reside in this file.
	 *
	 * Note that this assume you're going to use jQuery, so it prepares
	 * the $ function reference to be used within the scope of this
	 * function.
	 *
	 * From here, you're able to define handlers for when the DOM is
	 * ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * Or when the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and so on.
	 *
	 * Remember that ideally, we should not attach any more than a single DOM-ready or window-load handler
	 * for any particular page. Though other scripts in WordPress core, other plugins, and other themes may
	 * be doing this, we should try to minimize doing that in our own work.
	 */
	 jQuery(function(){
	 	if(jQuery('.spinner').length){
	 		jQuery(".spinner").each(function(e){
	 			jQuery(this).spinner();
	 		});
	 	}
	 	if(jQuery(".timepicker").length){
	 		jQuery('.timepicker').each(function(e){
	 			jQuery(this).timepicker();
	 		});
	 	}
	 });

	// jQuery('.'+cb_hm_ajaxobj.name+'_cb_hm_upload_image_button').click(function() {
	// 	var v = jQuery(this);console.log('hm');
	// 	v.closest('.room_images').find('.active_img').removeClass('active_img');
	// 	v.stop().addClass('active_img');
	// 	fileInput = v.closest('.room_images').find('input.uploadfield');
	// 	//console.log(fileInput);
	// 	post_id = jQuery('#post_ID').val();
		
	// 	if(typeof post_id == 'undefined') post_id = 0; 

	// 	tb_show('', 'media-upload.php?post_id='+post_id+'&amp;type=image&amp;TB_iframe=true');
	// });
	// window.original_send_to_editor = window.send_to_editor;
	// window.send_to_editor = function(html){

	// 	if (fileInput) {
	// 		fileurl = jQuery('img',html).attr('src');

	// 		fileInput.closest('.room_images').find('.active_img').parent().css({backgroundImage:'url('+fileurl+')'}).html('<div class="image_editor_box"><a href="JavaScript:void(0)" class="cb_hm_change_image">change</a><a href="JavaScript:void(0)" class="cb_hm_remove_image"><i class="fa fa-trash"></i></a>');

	// 		tb_remove();

	// 	} else {
	// 		window.original_send_to_editor(html);
	// 	}
	// };

	 jQuery(document).on('click', '.cb_hm .cb_hm_ajax_submit', function(e){
	 	var form = jQuery(this).closest('form');
	 	var formaction = form.find('input[name="action"]').val();
	 	var resetform = false,
	 		clearform = false;
	 	switch(formaction){
	 		case cb_hm_ajaxobj.name+'_general_settings':
	 			updatecontent('reservation_policy');
	 			updatecontent('custom_cancelllation_policy');
	 			updatecontent('email_message');
	 			updatecontent('propertydescription');
	 			break;
	 		case cb_hm_ajaxobj.name+'_edit_room':
	 		case cb_hm_ajaxobj.name+'_new_room':
	 			updatecontent('long_description');
	 			// updatecontent('email_message');
	 			break;
	 		default:
	 			break;
	 	}
	 	if(form.valid()){
	 		var options = { 
			  beforeSubmit:  function(){
			  	var data = {
			  		'status': 'loading',
			  		'message': 'Please wait...'
			  	};
			  	jQuery('body').append('<div class="cb_hm_ajax_message" />');
			  	var template = Handlebars.compile(cb_hm_ajax_commonmessage);
			    jQuery('.cb_hm_ajax_message').html(template(data));
			    show_cb_hm_message();
			  }, 
			  success:       function(json){
			    // jQuery("body").find('.cb_hm_ajaxloader').remove();
			    if(json.success){
			      switch(formaction){
			        case cb_hm_ajaxobj.name+'_new_room':
			          var template = Handlebars.compile(cb_hm_ajax_commonmessage);
			          jQuery('.cb_hm_ajax_message').html(template(json));
			          show_cb_hm_message();
			          window.setTimeout(function() {jQuery('.cb_hm_ajaxloader').stop().animate({top:'-200px'}, 400, function(){jQuery("body").find('.cb_hm_ajaxloader').stop().remove();});}, 2000);
			          document.location.href = cb_hm_ajaxobj.admin+'admin.php?page=mybookings-rooms&subpage=edit-room&room_id='+json.room_id;
			          break;
			        case cb_hm_ajaxobj.name+'_updatecommonrate':
			 			form.attr('action', '');
			 			jQuery('.cb_hm .updateRateperiod').click();
			 			break;
			        case cb_hm_ajaxobj.name+'_updatecommonsell':
			 			form.attr('action', '');
			 			jQuery('.cb_hm .updateSellperiod').click();
			 			break;
			        default:
			          var template = Handlebars.compile(cb_hm_ajax_commonmessage);
			          jQuery('.cb_hm_ajax_message').html(template(json));
			          show_cb_hm_message();
			          window.setTimeout(function() {jQuery('.cb_hm_ajax_message').stop().animate({top:'-200px'}, 400, function(){jQuery("body").find('.cb_hm_ajax_message').stop().remove();});}, 2000);
			          break;
			      }
			    }else {
			      switch(formaction){
			        default:
			          if(json == '-1'){
			            jQuery("body").find('.cb_hm_ajaxloader').html("<div class='alert msgsboard alert-danger'>Something is wrong with form submission, please reload the page</div>");
			          }else {
			            var template = Handlebars.compile(cb_hm_ajax_commonmessage);
			           	jQuery('.cb_hm_ajax_message').html(template(data));
			          }
			          break;
			      }
			    }
			  },
			  error:function(){
			    jQuery("body").find('.cb_hm_ajaxloader').stop({});
			  },
			  url:       form.attr('action'), 
			  type:      'post',
			  dataType:  'json',
			  clearForm: clearform,
			  resetForm: resetform,
			}; 

			form.ajaxSubmit(options);
	 	}else {
	 		form.find('input.error:eq(0)').focus();
	 	}
	 });

	jQuery(document).on('click','#wp-mybookings .plugin-description a', function(e){
		e.preventDefault();
		jQuery.ajax({
			url:'https://www.cloudbeds.com/wp-plugins/mybookings-light/update.php',
			data: {action:'callback', version:cb_hm_ajaxobj.version},
			async: false,
			contentType: "application/json",
			dataType:'jsonp',
			beforeSend:function(){
				var data = {
			  		'status': 'loading',
			  		'message': 'Please wait...'
			  	};
			  	jQuery('body').append('<div class="cb_hm_ajax_message" />');
			  	var template = Handlebars.compile(cb_hm_ajax_commonmessage);
			    jQuery('.cb_hm_ajax_message').html(template(data));
			    show_cb_hm_message();
			},
			success:function(json){
				console.log(json.message);
		 		var template = Handlebars.compile(cb_hm_ajax_commonmessage);
	          	jQuery('.cb_hm_ajax_message').html(template(json));
	          	show_cb_hm_message();
	          	window.setTimeout(function() {jQuery('.cb_hm_ajax_message').stop().animate({top:'-200px'}, 400, function(){jQuery("body").find('.cb_hm_ajax_message').stop().remove();});}, 10000);
			},
			error:function(){
				var data = {
			  		'status': 'danger',
			  		'message': 'Checking Failed!!!'
			  	};
			  	jQuery('body').append('<div class="cb_hm_ajax_message" />');
			  	var template = Handlebars.compile(cb_hm_ajax_commonmessage);
			    jQuery('.cb_hm_ajax_message').html(template(data));
			    show_cb_hm_message();
			    window.setTimeout(function() {jQuery('.cb_hm_ajax_message').stop().animate({top:'-200px'}, 400, function(){jQuery("body").find('.cb_hm_ajax_message').stop().remove();});}, 2000);
			}    
		});
		// jQuery.get('https://www.cloudbeds.com/wp-plugins/mybookings-light/update.php', {'action':'info'}, function(response){
		// 	console.log(response);
		// });
	});

	function show_cb_hm_message(){
		var wintop = (jQuery(window).height() - jQuery('.cb_hm_ajax_message_container').outerHeight())/2;
		var winleft = (jQuery(window).width() - jQuery('.cb_hm_ajax_message_container').outerWidth() - 25)/2;
		jQuery('.cb_hm_ajax_message_container').css({left:winleft});
		jQuery('.cb_hm_ajax_message_container').stop().animate({top:wintop}, 300);
	}

	function updatecontent(id){
		var content = '';
		if(jQuery('#wp-'+id+'-wrap').hasClass('html-active')){
			content = jQuery('#'+id).val();
		}else{
			content = tinyMCE.editors[id].getContent();
		}
		jQuery('#'+id).val(content);
	}

})( jQuery );
